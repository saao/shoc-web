module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        less: {
            dist: {
                options: {
                    compress: true
                },
                files: {
                    './app/static/css/app.css': './app/static/src/less/app.less'
                }
            }
        },

        concat: {
            dist: {
                files: {
                    './app/static/js/app.js': [
                        './bower_components/jquery/dist/jquery.js',
                        './bower_components/underscore/underscore.js',
                        './bower_components/backbone/backbone.js',
                        './bower_components/bootstrap/dist/js/bootstrap.js',
                        './app/static/src/js/app.js',
                    ],
                    './app/static/js/filterwheel.js': [
                        './bower_components/bootstrap-spinedit/js/bootstrap-spinedit.js',
                        './bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
                        './app/static/src/js/filterwheel.js'
                    ],
                    './app/static/js/gps.js': [
                        './bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
                        './bower_components/d3/d3.js',
                        './app/static/src/js/gps.js'
                    ],
                    './app/static/js/camera.js': [
                        './app/static/js9/js/jquery.contextMenu.js',
                        './app/static/js9/js/jquery.flot.js',
                        './app/static/js9/js/jquery.flot.errorbars.js',
                        './app/static/js9/js/jquery.flot.selection.js',
                        './app/static/js9/js/jquery.doubletap.js',
                        './app/static/js9/js/flot-zoom.js',
                        './app/static/js9/js/sprintf.js',
                        './app/static/js9/js/dhtmlwindow.js',
                        './app/static/js9/js/dhtmlwindow_blurb.js',
                        './app/static/js9/js/fabric.js',
                        './app/static/js9/js/pako_inflate.js',
                        './app/static/js9/js/FileSaver.min.js',
                        './app/static/js9/js/astroem.js',
                        './app/static/js9/js9.min.js',
                        './app/static/js9/js9plugins.js',
                        './app/static/src/js/js9.lightcurve.js',
                        './app/static/src/js/js9.lightcurve-bscounts.js',
                        './app/static/src/js/js9.regionstats.js',
                        './app/static/src/js/js9.crosshair.js',
                        './app/static/src/js/js9.subimage.js',
                        './app/static/src/js/camera.js',
                        './app/static/src/js/scripts.js'
                    ]
                }
            }
        },

        uglify: {
            dist: {
                files: {
                    './app/static/js/app.js': './app/static/js/app.js',
                    './app/static/js/filterwheel.js': './app/static/js/filterwheel.js',
                    './app/static/js/gps.js': './app/static/js/gps.js',
                }
            }
        },

        watch: {
            less: {
                options: {
                    livereload: true
                },
                files: ['./app/static/src/less/*.less'],
                tasks: ['less']
            },
            js: {
                options: {
                    livereload: true
                },
                files: [
                    './app/static/src/js/*.js',
                    './app/static/js9/*.js'
                ],
                tasks: ['concat']
            },
            html: {
                options: {
                    livereload: true
                },
                files: ['./app/templates/**/*.html']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build', ['less', 'concat', 'uglify']);
}
