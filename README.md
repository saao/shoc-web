# SHOC Web Interface

A web interface with which to operate the SHOC instrument.


## Develop

The first step for a new developer to get started is to make a local
clone of the git repository:

    $ git clone https://bitbucket.org/saao/shoc-web


### Dependencies

Server-side dependencies can be installed using pip:

    $ pip install -r requirements.txt

Client-side dependencies are managed with Bower and built using Grunt:

    $ npm install -g bower
    $ npm install -g grunt-cli
    $ npm install

Note that these are Node-based tools that require the package manager
`npm` which is bundled with Node.

You're now ready to install the client-side dependencies:

    $ bower install

When making changes to any files inside of `app/static/src` you will
need to rebuild these resources for your changes to take effect:

    $ grunt build


### Configuration

The newly checked out code contains a sample configuration file which
needs to be copied and modified. First, make a copy of the file:

    $ cp app/settings.cfg.sample app/settings.cfg

Then modify the `SHOC_HOST` setting to point to the host machine where
the SHOC servers are running.

It's sometimes useful to put the system in debug mode. This can be done
by adding an additional setting called `DEBUG` and setting that to True
which will enable debug logging that will appear in your terminal when
running the web server.


### Running the Web Server

The web server can be started by running the following command:

    $ gunicorn --worker-class flask_sockets.worker 'app:create_app()' -b 0.0.0.0:5000

The reason for using gunicorn instead of the Flask development server,
even during development, is because the Flask server does not support
Websockets and the camera interface needs a Websocket connection to send
camera data to the browser.

If you don't need to use the camera interface and would like to take
advantage of some of Flask's debugging features you can just as easily
run that:

    $ python manage.py runserver

In both cases, the SHOC web interface should now be running at
http://localhost:5000/


## Testing

Functional and unit tests can be run manually with the following command:

    $ python manage.py test


## Deploy

Deploying the application to a new production server is done in much the same
way as you would when setting it up for development.

Create a local clone of the code into `/var/www/`:

    $ cd /var/www/ && git clone https://bitbucket.org/saao/shoc-web


The application will be run as the `www-data` user so make sure that the owner
is set correctly:

    $ chown -R www-data: shoc-web

Install the server-side dependencies:

    $ cd /var/www/shoc-web && pip install -r requirements.txt

Make a copy of the file configuration file:

    $ cp app/settings.cfg.sample app/settings.cfg

Then modify the SHOC_HOST setting to point to the host machine where the SHOC servers are running.

Make sure that the logging directory exists and that it is owned by `www-data`:

    $ mkdir /var/log/shoc-web && chown www-data: /var/log/shoc-web

We make use of an Upstart script to manage the web server process. This
can be copied from the project directory:

    $ cp etc/init/shoc-web.conf /etc/init/

The web server process can now be managed using the `service` command:

    $ service shoc-web start
    $ service shoc-web restart
    $ service shoc-web stop
    $ service shoc-web status


## Other Documentation

- [Website](http://shoc.saao.ac.za)
- [For Users and Technicians](http://topswiki.saao.ac.za/index.php/SHOC)
- [Data Reduction Pipeline](http://marissa.saao.ac.za/SHOCpipeline/)


### For Developers

- [Instrumentation Library](https://bitbucket.org/saao/shoc)
- [Flask](http://flask.pocoo.org)
- [Node](http://nodejs.org)
- [Bower](http://bower.io)
- [Grunt](http://gruntjs.com)
- [Bootstrap](http://getbootstrap.com)
- [Backbone](http://backbonejs.org)
- [Upstart](http://upstart.ubuntu.com)