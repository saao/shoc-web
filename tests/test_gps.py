"""Automated unit tests for the GPS component of SHOC."""

import json
import datetime
import unittest

from gevent import timeout
from flask import current_app, url_for


class GPSTestCase(unittest.TestCase):
    def setUp(self):
        self.client = current_app.test_client()

    def tearDown(self):
        pass

    def test_index(self):
        """Tests that the main page is accessible."""
        with timeout.Timeout(5):
            response = self.client.get(url_for('gps.index'))

        assert response.status_code == 200
        assert response.mimetype == 'text/html'

    def test_get_status(self):
        """Tests the GET endpoint of the GPS API."""
        with timeout.Timeout(5):
            response = self.client.get(url_for('gps.api.get'))

        assert response.status_code == 200
        assert response.mimetype == 'application/json'

        gps = json.loads(response.data.decode('utf-8'))

        attributes = [
            'antenna',
            'local_datetime',
            'oscillator_mode',
            'pop',
            'position',
            'satellites',
            'timing_status',
            'univeral_datetime',
            'user_time_bias',
        ]

        assert all(attribute in gps for attribute in attributes)

    def test_set_pop_invalid(self):
        """Sets the POP to a date in the past."""
        with timeout.Timeout(5):
            response = self.client.patch(url_for('gps.api.patch'), data=json.dumps({
                'pop': {
                    'mode': 2,  # Repeat
                    'pulse_width': 1,  # 10 microseconds
                    'repeat_interval': 200,  # 200 milliseconds
                    'start_date': '01/01/1970',
                    'start_time': '00:00:00.0',
                }
            }))

        assert response.status_code == 400
        assert response.mimetype == 'application/json'

    def test_set_pop_valid(self):
        """Sets the POP to a date in the future."""
        now_plus_one_hour = datetime.datetime.now() + datetime.timedelta(hours=1)
        with timeout.Timeout(5):
            response = self.client.patch(url_for('gps.api.patch'), data=json.dumps({
                'pop': {
                    'mode': 2,  # Repeat
                    'pulse_width': 1,  # 10 microseconds
                    'repeat_interval': 200,
                    'start_date': now_plus_one_hour.strftime('%m/%d/%Y'),
                    'start_time': now_plus_one_hour.strftime('%H:%M:%S.0'),
                }
            }))

        assert response.status_code == 200
        assert response.mimetype == 'application/json'


if __name__ == '__main__':
    unittest.main()
