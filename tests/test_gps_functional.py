"""Functional tests for the GPS component."""

import time
import datetime
import unittest

from flask import url_for
from selenium import webdriver
from app import create_app
from fields import TextField, SelectField


class GPS(object):
    mode = SelectField('select[name="mode"]')
    pulse_width = SelectField('select[name="pulse_width"]')
    start_date = TextField('input[name="start_date"]')
    start_time = TextField('input[name="start_time"]')
    repeat_interval = TextField('input[name="repeat_interval"]')

    MODE_ONESHOT = 1
    MODE_REPEAT = 2

    def __init__(self, driver):
        self.driver = driver

    @property
    def pulse(self):
        return self.driver.find_element_by_css_selector(
            '#pop-status').text

    @property
    def timing(self):
        return self.driver.find_element_by_css_selector(
            '#timing-status').text

    @property
    def reference(self):
        return self.driver.find_element_by_css_selector(
            '#reference-status').text

    @property
    def antenna(self):
        return self.driver.find_element_by_css_selector(
            '#antenna-status').text

    def save(self):
        self.driver.find_element_by_css_selector(
            'button.btn-save').click()

        # Wait for the changes to take effect.
        time.sleep(2)

    def stop(self):
        self.driver.find_element_by_css_selector(
            'button.btn-stop').click()

        # Wait for the changes to take effect.
        time.sleep(2)

class GPSTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.driver = webdriver.Firefox()
        except:
            pass

        if cls.driver:
            cls.driver.set_window_position(0, 0)
            cls.driver.maximize_window()

            cls.app = create_app()
            cls.app_context = cls.app.app_context()
            cls.app_context.push()

            cls.driver.get(url_for('gps.index', _external=True))

            # Wait for the page to load before continuing.
            time.sleep(2)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.app_context.pop()

    def test_status_indicators(self):
        """Checks that the status indicators are all valid."""
        self.assertIn('GPS', self.driver.title)

        gps = GPS(self.driver)

        assert gps.timing == 'Valid'
        assert gps.reference == 'Ready'
        assert gps.antenna == 'Good'

    def test_set_invalid_pop(self):
        """Sets the POP with invalid data."""
        self.assertIn('GPS', self.driver.title)

        gps = GPS(self.driver)

        gps.mode = gps.MODE_REPEAT
        gps.pulse_width = 1  # 10 microseconds

        gps.start_date = ''
        gps.start_time = ''
        gps.repeat_interval = ''

        gps.save()

        assert 'Please correct the errors below and try again.' in self.driver.page_source
        assert 'Please enter a valid start date.' in self.driver.page_source
        assert 'Please enter a valid start time.' in self.driver.page_source
        assert 'Please enter a valid repeat interval.' in self.driver.page_source

    def test_repeat_pop(self):
        """Tests the POP in repeat mode."""
        self.assertIn('GPS', self.driver.title)

        gps = GPS(self.driver)

        gps.mode = gps.MODE_REPEAT
        gps.pulse_width = 1

        now_plus_5_seconds = (datetime.datetime.now()
            + datetime.timedelta(seconds=5))

        gps.start_date = now_plus_5_seconds.strftime('%m/%d/%Y')
        gps.start_time = now_plus_5_seconds.strftime('%H:%M:%S.0')
        gps.repeat_interval = 200

        gps.save()

        assert gps.pulse == 'Pending'

        # Wait for the POP to activate.
        time.sleep(6)

        assert gps.pulse == 'Activated'

    def test_oneshot_pop(self):
        """Tests the POP in one-shot mode."""
        self.assertIn('GPS', self.driver.title)

        gps = GPS(self.driver)

        # TODO: Repeat interval field should be disabled.

        gps.mode = gps.MODE_ONESHOT
        gps.pulse_width = 1

        now_plus_5_seconds = (datetime.datetime.now()
            + datetime.timedelta(seconds=5))

        gps.start_date = now_plus_5_seconds.strftime('%m/%d/%Y')
        gps.start_time = now_plus_5_seconds.strftime('%H:%M:%S.0')

        gps.save()

        assert gps.pulse == 'Pending'

        time.sleep(6)

        assert gps.pulse == 'Activated'

    def test_stop_pop(self):
        """Stops the currently scheduled pulse."""
        self.assertIn('GPS', self.driver.title)

        gps = GPS(self.driver)

        gps.mode = gps.MODE_REPEAT
        gps.pulse_width = 1

        now_plus_5_seconds = (datetime.datetime.now()
            + datetime.timedelta(seconds=5))

        gps.start_date = now_plus_5_seconds.strftime('%m/%d/%Y')
        gps.start_time = now_plus_5_seconds.strftime('%H:%M:%S.0')
        gps.repeat_interval = 200

        gps.save()

        assert gps.pulse == 'Pending'

        time.sleep(6)

        assert gps.pulse == 'Activated'

        gps.stop()

        assert gps.pulse == 'Off'
