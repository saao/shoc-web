import unittest
from flask import current_app


class SHOCWebTestcase(unittest.TestCase):
    def setUp(self):
        self.app = current_app.test_client()

    def tearDown(self):
        pass

    def test_noop(self):
        assert True
