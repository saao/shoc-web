"""Functional tests for the camera component."""

import time
import datetime
import unittest

from flask import url_for
from selenium import webdriver
from app import create_app
from test_gps_functional import GPS
from fields import TextField, SelectField, RadioField


# TODO: Test changing temperature.
# TODO: Test preview mode.
# TODO: Test readout rate values based on output amplifier
# TODO: Test preamp gain values based on readout rate.


class Camera(object):
    trigger = SelectField('#trigger_mode')
    exposure = TextField('#exposure_time')
    kinetic_series_length = TextField('#kinetic_series_length')
    subframe = SelectField('#image-sub_image')
    binning = SelectField('#image-binning')
    readout_rate = SelectField('#horizontal_shift_speed')
    preamp_gain = SelectField('#preamp_gain')
    em_gain = TextField('#em_gain')
    output_amplifier = RadioField('input[name="output_amplifier"]')
    temperature = TextField('#target_temperature')
    start_index = TextField('#start_index')

    TRIGGER_INTERNAL = 0
    TRIGGER_EXTERNAL = 1
    TRIGGER_EXTERNAL_START = 6

    AMPLIFIER_EM = 0
    AMPLIFIER_CONVENTIONAL = 1

    def __init__(self, driver):
        self.driver = driver

    @property
    def kinetic_cycle_time(self):
        return self.driver.find_element_by_css_selector(
            '#kinetic_cycle_time').get_attribute('value')

    @property
    def progress(self):
        return self.driver.find_element_by_css_selector(
            '.progress-bar span').text

    @property
    def save_name(self):
        return self.driver.find_element_by_css_selector(
            '#save_name').text

    def turn_on(self):
        self.driver.find_element_by_css_selector(
            'button.btn-on').click()

        # Wait for the camera to turn on and for the page to reload.
        time.sleep(5)

    def turn_off(self):
        self.driver.find_element_by_css_selector(
            '[data-target="#confirmation"]').click()

        # Wait for the confirmation dialog to appear.
        time.sleep(1)

    def turn_off_confirm(self):
        self.driver.find_element_by_css_selector(
            'button.btn-off').click()

        # Wait for the dialog to fade out.
        time.sleep(1)

    def em_mode_confirm(self):
        self.driver.find_element_by_css_selector(
            'button.btn-enable-em').click()

    def start_index_confirm(self):
        self.driver.find_element_by_css_selector(
            'button.btn-set-start-index').click()

    def start_preview(self):
        self.driver.find_element_by_css_selector(
            'button.btn-preview').click()

    def start(self):
        self.driver.find_element_by_css_selector(
            'button.btn-start').click()

    def stop(self):
        self.driver.find_element_by_css_selector(
            'button.btn-stop').click()


class CameraTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.driver = webdriver.Firefox()
        except:
            pass

        if cls.driver:
            cls.driver.set_window_position(0, 0)
            cls.driver.maximize_window()

            cls.app = create_app()
            cls.app_context = cls.app.app_context()
            cls.app_context.push()

            cls.driver.get(url_for('camera.index', _external=True))

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.app_context.pop()

    def test_internal(self):
        """Tests the camera with internal triggering."""
        self.assertIn('Camera', self.driver.title)

        camera = Camera(self.driver)

        camera.turn_on()

        camera.trigger = camera.TRIGGER_INTERNAL
        camera.exposure = 1
        camera.kinetic_series_length = 100
        camera.subframe = 3  # 128x128
        camera.binning = 1  # 2x2

        camera.start()

        time.sleep(5)

        assert camera.progress == '100'

        camera.turn_off()
        camera.turn_off_confirm()

    def test_external(self):
        """Tests the camera (and GPS) with external triggering."""
        self.assertIn('Camera', self.driver.title)

        camera = Camera(self.driver)

        camera.turn_on()

        # TODO: Exposure time field should be disabled.

        camera.trigger = camera.TRIGGER_EXTERNAL
        camera.kinetic_series_length = 10
        camera.subframe = 0
        camera.binning = 0

        # Go to the GPS interface and schedule a pulse.
        self.driver.get(url_for('gps.index', _external=True))

        gps = GPS(self.driver)

        gps.mode = gps.MODE_REPEAT
        gps.pulse_width = 1

        now_plus_10_seconds = (datetime.datetime.now()
            + datetime.timedelta(seconds=10))

        gps.start_date = now_plus_10_seconds.strftime('%m/%d/%Y')
        gps.start_time = now_plus_10_seconds.strftime('%H:%M:%S.0')
        gps.repeat_interval = 200

        gps.save()

        assert gps.pulse == 'Pending'

        self.driver.get(url_for('camera.index', _external=True))

        # Wait for the GPS to trigger.
        camera.start()

        time.sleep(5)

        assert camera.progress == '10'

        camera.turn_off()
        camera.turn_off_confirm()

    def test_external_start(self):
        """Tests the camera (and GPS) with external start triggering."""
        self.assertIn('Camera', self.driver.title)

        camera = Camera(self.driver)

        camera.turn_on()

        camera.trigger = camera.TRIGGER_EXTERNAL_START
        camera.exposure = 1
        camera.kinetic_series_length = 10
        camera.subframe = 0
        camera.binning = 0

        self.driver.get(url_for('gps.index', _external=True))

        gps = GPS(self.driver)

        gps.mode = gps.MODE_REPEAT
        gps.pulse_width = 1

        now_plus_10_seconds = (datetime.datetime.now()
            + datetime.timedelta(seconds=10))

        gps.start_date = now_plus_10_seconds.strftime('%m/%d/%Y')
        gps.start_time = now_plus_10_seconds.strftime('%H:%M:%S.0')
        gps.repeat_interval = 200

        gps.save()

        assert gps.pulse == 'Pending'

        self.driver.get(url_for('camera.index', _external=True))

        # Wait for the GPS to trigger.
        camera.start()

        time.sleep(20)

        assert camera.progress == '10'

        camera.turn_off()
        camera.turn_off_confirm()

    def test_external_without_pop(self):
        """Tests external mode without a scheduled pulse."""
        self.assertIn('Camera', self.driver.title)

        camera = Camera(self.driver)
        camera.turn_on()
        camera.trigger = camera.TRIGGER_EXTERNAL

        self.driver.get(url_for('gps.index', _external=True))

        gps = GPS(self.driver)
        gps.stop()

        assert gps.pulse == 'Off'

        self.driver.get(url_for('camera.index', _external=True))

        camera.start()

        assert 'POP state is not pending' in self.driver.page_source

        camera.turn_off()
        camera.turn_off_confirm()

    def test_start_index_confirm(self):
        """Tests the start index confirmation dialog."""
        self.assertIn('Camera', self.driver.title)

        camera = Camera(self.driver)
        camera.turn_on()

        # Go to the advanced tab.
        self.driver.find_element_by_css_selector(
            'a#tab-advanced').click()

        old_index = int(camera.start_index)

        # Incrementing the start index should be okay.
        camera.start_index = old_index + 1

        assert camera.start_index == old_index + 1

        # When we decrement the start index we need to confirm the change.
        camera.start_index = old_index - 1

        camera.start_index_confirm()

        assert camera.start_index == old_index - 1

        camera.turn_off()
        camera.turn_off_confirm()

    def test_em_mode_confirm(self):
        """Tests the EM mode confirmation dialog."""
        self.assertIn('Camera', self.driver.title)

        camera = Camera(self.driver)
        camera.turn_on()

        camera.output_amplifier = camera.AMPLIFIER_EM

        camera.em_mode_confirm()

        assert camera.output_amplifier == camera.AMPLIFIER_EM

        camera.turn_off()
        camera.turn_off_confirm()
