"""Functional tests for the filter wheel component."""

import time
import unittest

from flask import url_for
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from app import create_app


class Page(object):
    def __init__(self, driver):
        self.driver = driver
        self.wheels = []

        for form in self.driver.find_elements_by_css_selector('form'):
            self.wheels.append(Wheel(form))

    def reset_confirm(self):
        """Confirms a wheel reset.

        The reset confirmation is displayed in a modal dialog so it needs to
        happen at the page level.
        """
        self.driver.find_element_by_css_selector(
            'button.btn-reset').click()


class Wheel(object):
    """Represents an individual wheel on the page."""
    def __init__(self, form):
        self.form = form

    def __getattr__(self, name):
        """Proxies attribute access to the form element."""
        return getattr(self.form, name)

    @property
    def is_initialized(self):
        try:
            self.find_element_by_css_selector(
                '.status-initialized.label-success')
        except NoSuchElementException:
            return False
        else:
            return True

    @property
    def at_reference(self):
        try:
            self.find_element_by_css_selector(
                '.status-reference.label-success')
        except NoSuchElementException:
            return False
        else:
            return True

    @property
    def is_centered(self):
        try:
            self.find_element_by_css_selector(
                '.status-centered.label-success')
        except NoSuchElementException:
            return False
        else:
            return True

    @property
    def position(self):
        return self.find_element_by_css_selector(
            'input[name="position"]').get_attribute('value')

    def next(self):
        self.find_element_by_css_selector(
            '.glyphicon-chevron-up').click()

    def initialize(self):
        self.find_element_by_css_selector(
            'button.btn-initialize').click()

    def move(self):
        self.find_element_by_css_selector(
            'button.btn-move').click()

    def reset(self):
        self.find_element_by_css_selector(
            '[data-target="#reset-confirmation"]').click()

    def toggle_advanced_options(self):
        self.find_element_by_css_selector(
            '.advanced-toggle').click()


class FilterWheelTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.driver = webdriver.Firefox()
        except:
            pass

        if cls.driver:
            cls.driver.set_window_position(0, 0)
            cls.driver.maximize_window()

            cls.app = create_app()
            cls.app_context = cls.app.app_context()
            cls.app_context.push()

            cls.driver.get(url_for('filterwheel.index', _external=True))

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.app_context.pop()

    def test_initialize(self):
        """Initializes all wheels."""
        self.assertIn('Filter Wheel', self.driver.title)

        page = Page(self.driver)

        for wheel in page.wheels:
            wheel.initialize()

            # TODO: Check that the 'moving' indicator is active.

            assert wheel.position == '1'

            # When a wheel is initialized it also needs to indicate that it is
            # at the reference position and that it is centered.
            assert wheel.is_initialized
            assert wheel.at_reference
            assert wheel.is_centered

    def test_move(self):
        """Moves all wheels to the next position."""
        self.assertIn('Filter Wheel', self.driver.title)

        page = Page(self.driver)

        for wheel in page.wheels:
            # Move the wheel to the next position.
            wheel.next()
            wheel.move()

            # TODO: Check that the 'moving' indicator is active.

            # The wheel should now be in position 2.
            assert wheel.position == '2'

            # When a wheel is moved to a position other than the reference
            # position, it is no longer at the reference position but should
            # still be centered.
            assert not wheel.at_reference
            assert wheel.is_centered

    def test_reset(self):
        """Resets all wheels."""
        self.assertIn('Filter Wheel', self.driver.title)

        page = Page(self.driver)

        for wheel in page.wheels:
            wheel.toggle_advanced_options()

            wheel.reset()

            # Wait for the modal dialog to appear.
            time.sleep(1)

            page.reset_confirm()

            # Wait for a bit for the modal dialog to fade out.
            time.sleep(1)

            # When we reset the wheel status it should no longer indicate that
            # it has been initialized or that it is centered.
            assert not wheel.is_initialized
            assert not wheel.is_centered
