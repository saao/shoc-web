import unittest
import json

from gevent import timeout
from flask import current_app, url_for


class CameraTestCase(unittest.TestCase):
    """Tests the camera API."""

    def setUp(self):
        self.client = current_app.test_client()

    def tearDown(self):
        assert False

    def _test_uninitializd(self):
        with timeout.Timeout(5):
            response = self.client.get(url_for('camera.index'))

        assert response.status_code == 200
        assert response.mime_type == 'text/html'

    def _test_initialized(self):
        assert False

    def _test_set_trigger_mode(self):
        assert False

    def _test_set_exposure_time(self):
        assert False

    def _test_set_kinetic_series_length(self):
        assert False

    def _test_set_temperature(self):
        assert False

    def _test_set_start_index(self):
        assert False

    def _test_set_image(self):
        assert False

    def _test_set_horizontal_shift_speed(self):
        assert False

    def _test_set_output_amplifier(self):
        assert False

    def _test_set_preamp_gain(self):
        assert False

    def _test_set_em_gain(self):
        assert False


if __name__ == '__main__':
    unittest.main()
