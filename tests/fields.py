"""Reusable fields that help cut down on repetition in tests."""

from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys


class TextField(object):
    def __init__(self, selector):
        self.selector = selector

    def __get__(self, obj, objtype):
        return obj.driver.find_element_by_css_selector(
            self.selector).get_attribute('value')

    def __set__(self, obj, value):
        element = obj.driver.find_element_by_css_selector(self.selector)

        # Manually add a backspace for each of the characters in the
        # input field. We use this method instead of clear() to more
        # accurately simulate a typical user's behaviour and because
        # calling clear() and then entering the appropriate string into
        # the field will trigger the onchange handler twice.
        keys = [Keys.BACKSPACE for _ in element.get_attribute('value')]

        # Queue the sent string that should be entered into the field.
        keys.append(str(value))

        # Enter all the queued keys at once.
        element.send_keys(keys)


class SelectField(object):
    def __init__(self, selector):
        self.selector = selector

    def __get__(self, obj, objtype):
        return obj.driver.find_element_by_css_selector(
            self.selector).get_attribute('value')

    def __set__(self, obj, value):
        Select(obj.driver.find_element_by_css_selector(
            self.selector)).select_by_value(str(value))


class RadioField(object):
    def __init__(self, selector):
        self.selector = selector

    def __get__(self, obj, objtype):
        options = obj.driver.find_elements_by_css_selector(
            self.selector)

        for option in options:
            if option.is_selected():
                return option.get_attribute('value')

    def __set__(self, obj, value):
        options = obj.driver.find_elements_by_css_selector(
            self.selector)

        for option in options:
            if option.get_attribute('value') == str(value):
                option.click()
