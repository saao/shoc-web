"""Automated unit tests for the filter wheel component of SHOC."""

import unittest
import json
import time

from gevent import timeout
from flask import current_app, url_for
from app import create_app


class FilterWheelTestCase(unittest.TestCase):
    """Tests the filter wheel API."""

    def setUp(self):
        self.client = current_app.test_client()

    def tearDown(self):
        pass

    def _test_index(self):
        """Tests that the main page is accessible."""
        with timeout.Timeout(5):
            response = self.client.get(url_for('filterwheel.index'))

        assert response.status_code == 200
        assert response.mimetype == 'text/html'

    def _test_get_status(self):
        """Tests the GET endpoint of the filter wheel API."""
        with timeout.Timeout(5):
            response = self.client.get(url_for('filterwheel.api.get'))

        assert response.status_code == 200
        assert response.mimetype == 'application/json'

        wheels = json.loads(response.data.decode('utf-8'))

        assert 'wheels' in wheels and len(wheels['wheels']) == 2

        attributes = [
            'at_reference',
            'centered',
            'filters',
            'initialized',
            'moving',
            'position',
            'wheel',
            'wheel_id',
        ]

        for wheel in wheels['wheels']:
            assert all(attribute in wheel for attribute in attributes)

    def _test_initialize(self):
        """Tests the initialize command."""
        for wheel_id in ['A', 'B']:
            response = self.client.patch(
                url_for('filterwheel.api.patch', wheel_id=wheel_id),
                data=json.dumps({
                    'initialized': True
                }),
                headers={
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                })

            assert response.status_code == 200
            assert response.mimetype == 'application/json'

    def _test_reset(self):
        """Tests the reset command."""
        with timeout.Timeout(5):
            response = self.client.get(url_for('filterwheel.api.get'))

        assert response.status_code == 200
        assert response.mimetype == 'application/json'

        wheels = json.loads(response.data.decode('utf-8'))

        for wheel in wheels['wheels']:
            response = self.client.patch(
                url_for('filterwheel.api.patch', wheel_id=wheel['wheel_id']), data=json.dumps({
                    'reset': True
                }))

            assert response.status_code == 200
            assert response.mimetype == 'application/json'

            wheel = json.loads(response.data.decode('utf-8'))

            assert wheel['initialized'] == False

    def _test_move(self):
        """Tests the move command."""
        with timeout.Timeout(5):
            response = self.client.get(url_for('filterwheel.api.get'))

        assert response.status_code == 200
        assert response.mimetype == 'application/json'

        wheels = json.loads(response.data.decode('utf-8'))

        for wheel in wheels['wheels']:
            current_position = wheel['position']
            required_position = (current_position + 1) % 8

            response = self.client.patch(
                url_for('filterwheel.api.patch', wheel_id=wheel['wheel_id']), data=json.dumps({
                    'required_position': required_position
                }))

            assert response.status_code == 200
            assert response.mimetype == 'application/json'

            wheel = json.loads(response.data.decode('utf-8'))

            assert wheel['moving']

            # Wait for the filter wheel to get into the required position.
            time.sleep(10)

            response = self.client.get(url_for('filterwheel.api.get'))

            assert response.status_code == 200
            assert response.mimetype == 'application/json'

            wheel = json.loads(response.data.decode('utf-8'))

            assert wheel['position'] == required_position


if __name__ == '__main__':
    unittest.main()
