from http.client import NOT_FOUND, BAD_REQUEST
from flask import Blueprint, Response, render_template, request
from app import shoc, jsonify, login_required


views = Blueprint('filterwheel', __name__)


@views.route('/', methods=['GET'])
@login_required
def index():
    wheels = [w.__dict__ for w in shoc.filterwheel.wheels.values()]
    return render_template('filterwheel/index.html', wheels=wheels)


api = Blueprint('filterwheel_api', __name__)


@api.route('/', methods=['GET'])
@api.route('/<name>', methods=['GET'])
@login_required
def get(name=None):
    # XXX: Workaround for when no filter wheel devices are attached. When there
    # are no filter wheels attached, the controller sets shoc.filterwheel to an
    # empty dict which doesn't have a "get_state" attribute.
    try:
        wheels = shoc.filterwheel.get_state()
    except AttributeError:
        wheels = {}

    if name is None:
        return jsonify([w.__dict__ for w in wheels.values()])
    elif name in wheels:
        return jsonify(wheels[name].__dict__)
    return jsonify(), NOT_FOUND


@api.route('/<name>', methods=['PATCH'])
@login_required
def patch(name):
    if not request.json:
        return jsonify(), BAD_REQUEST

    # XXX: Workaround for when no filter wheel devices are attached. When there
    # are no filter wheels attached, the controller sets shoc.filterwheel to an
    # empty dict which doesn't have a "get_state" attribute.
    try:
        wheels = shoc.filterwheel.get_state()
    except AttributeError:
        wheels = {}

    if name not in wheels:
        return jsonify(), NOT_FOUND

    # Invoke a method on the filter wheel based on the data that was sent to
    # the web server. Only one operation can be executed per request, so if
    # multiple things are set the one that occurs first below will be executed.
    if 'initialized' in request.json:
        shoc.filterwheel.initialize(name)
    elif 'reset' in request.json:
        shoc.filterwheel.reset(name)
    elif 'required_position' in request.json:
        shoc.filterwheel.move(
            name, request.json['required_position'])
    else:
        return jsonify(), BAD_REQUEST

    return jsonify()
