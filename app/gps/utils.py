import datetime

from flask_babel import format_datetime, to_utc
from app import shoc


def get_status():
    status = shoc.gps.get_status()

    dt = datetime.datetime.strptime(status['dt'], '%Y-%m-%d %H:%M:%S')

    try:
        start_date = datetime.datetime.strptime(
            status['pop']['date'], '%m%d%Y').date()
        start_time = datetime.datetime.strptime(
            status['pop']['time'][:13], '%H%M%S.%f').time()
    except ValueError:
        # If we get invalid data back from the GPS (eg. '000000' for the date)
        # the above conversions will raise a ValueError. In those cases we just
        # display the date and time as-is.
        start_date = status['pop']['date']
        start_time = status['pop']['time']
    else:
        utc_dt = datetime.datetime.combine(start_date, start_time)
        start_date = format_datetime(utc_dt, 'MM/dd/YYYY')
        start_time = format_datetime(utc_dt, 'HH:mm:ss.SSSSSS')

    return {
        'local_datetime': format_datetime(dt, 'HH:mm:ss MM/dd/YYYY'),
        'universal_datetime': format_datetime(dt, 'HH:mm:ss MM/dd/YYYY', rebase=False),
        'timing_status': status['timing_status'],
        'pop': {
            'mode': status['pop']['status'],
            'start_date': start_date,
            'start_time': start_time,
            'pulse_width': status['pop']['width'],
            'repeat_interval': status['pop']['frequency'],
            'state': status['pop_state'],
        },
        'oscillator_mode': status['oscillator_mode'],
        'antenna': status['alarm']['antenna'],
        'position': {
            'latitude': "{degrees}&deg; {minutes}' {direction}".format(
                degrees=status['position'][0][:2],
                minutes=status['position'][0][2:],
                direction=status['position'][1]),
            'longitude': "{degrees}&deg; {minutes}' {direction}".format(
                degrees=status['position'][2][:3],
                minutes=status['position'][2][3:],
                direction=status['position'][3]),
        },
        'satellites': status['satellites'],
        'user_time_bias': status['user_time_bias'],
    }
