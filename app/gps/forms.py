import datetime

from werkzeug.datastructures import MultiDict
from flask_wtf import Form
from flask_babel import to_user_timezone, to_utc
from wtforms import SelectField, DateField, DateTimeField, IntegerField
from wtforms.validators import InputRequired, ValidationError
from app import shoc
from .models import lock


MODE_OFF = 0
MODE_REPEAT = 2
MODE_CHOICES = [
    (0, 'Off'),
    (1, 'One-Shot'),
    (2, 'Repeat'),
]


PULSE_WIDTH_10_MICROSECONDS = 1
PULSE_WIDTH_CHOICES = [
    (0, '1 microsecond'),
    (1, '10 microseconds (recommended)'),
    (2, '100 microseconds'),
    (3, '1 millisecond'),
    (4, '10 milliseconds'),
    (5, '50 milliseconds'),
    (6, '100 milliseconds'),
    (7, '250 milliseconds'),
    (8, 'Level Hold'),
]


def get_default_start_date():
    return to_user_timezone(datetime.datetime.utcnow())


# XXX: The three custom fields below catches and ignores any ValueErrors raised
# by their base classes during conversion. We then handle the validation
# conditionally in the actual form.

class StartTimeField(DateTimeField):
    def process_formdata(self, valuelist):
        try:
            super(StartTimeField, self).process_formdata(valuelist)
        except ValueError:
            self.data = None
        else:
            self.data = self.data.time()


class StartDateField(DateTimeField):
    def process_formdata(self, valuelist):
        try:
            super(StartDateField, self).process_formdata(valuelist)
        except ValueError:
            self.data = None


class RepeatIntervalField(IntegerField):
    def process_formdata(self, valuelist):
        try:
            super(RepeatIntervalField, self).process_formdata(valuelist)
        except ValueError:
            self.data = None


class POPForm(Form):
    """Programmed output pulse."""
    def __init__(self, formdata=None, *args, **kwargs):

        if formdata:
            # Make sure that the start time contains fractional seconds. This makes
            # data entry a bit more flexible so we can allow values like 'HH:MM:SS' or
            # 'HH:MM:SS.' where the fraction will default to zero.
            try:
                fractional_seconds = formdata['start_time'].split('.')[1]
            except IndexError:
                if formdata['start_time']:
                    formdata['start_time'] += '.0'
            else:
                if len(fractional_seconds) == 0:
                    formdata['start_time'] += '0'

        super(POPForm, self).__init__(MultiDict(formdata), *args, **kwargs)

    mode = SelectField('Mode', validators=[InputRequired()],
        choices=MODE_CHOICES, coerce=int)

    start_date = StartDateField('Start Date', default=get_default_start_date,
        format='%m/%d/%Y', description='Local Time (SAST)')
    start_time = StartTimeField('Start Time', format='%H:%M:%S.%f',
        description='Local Time (SAST)')

    pulse_width = SelectField('Pulse Width', validators=[InputRequired()],
        choices=PULSE_WIDTH_CHOICES,
        default=PULSE_WIDTH_10_MICROSECONDS, coerce=int)
    repeat_interval = RepeatIntervalField('Repeat Interval', default=0,
        description='Milliseconds')

    def validate_start_date(form, field):
        """Start date is not required when pulse is off."""
        if form.mode.data != MODE_OFF and field.data is None:
            raise ValidationError('Please enter a valid start date.')

    def validate_start_time(form, field):
        """Start time is not required when pulse is off."""
        if form.mode.data != MODE_OFF and field.data is None:
            raise ValidationError('Please enter a valid start time.')

    def validate_repeat_interval(form, field):
        """Repeat interval is only required when in repeat mode."""
        if form.mode.data == MODE_REPEAT and field.data is None:
            raise ValidationError('Please enter a valid repeat interval.')

        # Also make sure that the value is greater than one. The GPS driver
        # seems to choke on values less than that.
        if form.mode.data == MODE_REPEAT and field.data < 1:
            raise ValidationError('Please enter a value greater than 1.')

    def save(self):
        # Incoming dates are assumed to be in local time (SAST), so we need to
        # convert them to UTC before we send them to the GPS driver.
        try:
            local_dt = datetime.datetime.combine(
                self.start_date.data, self.start_time.data)
        except TypeError:
            # XXX: We bypass validation when the mode is being set to 'Off' to make
            # it easier for the user. That means that we sometimes get garbage data
            # in which case an exception is raised. Use the below values instead.
            start_date = '000000'
            start_time = '000000.000000'
        else:
            utc_dt = to_utc(local_dt)
            start_date = utc_dt.strftime('%m%d%Y')
            start_time = utc_dt.strftime('%H%M%S.%f')

        with lock:
            shoc.gps.set_pop(self.mode.data, start_date, start_time,
                str(self.repeat_interval.data), self.pulse_width.data)
