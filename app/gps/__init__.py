from socket import timeout

try:
    from http.client import BAD_REQUEST
except ImportError:
    from httplib import BAD_REQUEST

from flask import Blueprint, render_template, request, jsonify
from app import shoc, ValidationError, login_required
from .models import GPS
from .forms import POPForm


gps = Blueprint('gps', __name__)
model = GPS()


@gps.route('/', methods=['GET'])
@login_required
def index():
    form = POPForm(model.state['pop'])
    return render_template('gps/index.html', form=form, gps=model)


api = Blueprint('gps_api', __name__)


@api.route('/gps', methods=['GET'])
@login_required
def get():
    return model.state


@api.route('/gps', methods=['PATCH'])
@login_required
def patch():
    data = request.json.get('pop')
    if not data:
        return jsonify(), BAD_REQUEST

    form = POPForm(data)
    if not form.validate():
        raise ValidationError('Validation error.', form.errors)

    try:
        form.save()
    except timeout:
        raise Exception('Your request timed out. Please try again.')

    return model.state
