/* Allows the user to select a sub-image on the JS9 canvas. */

(function() {
    "use strict";

    function init() {
        this.id = null;

        // Adjust the region when changing the subframe or binning.
        $('#image-sub_image_left').change(function(e) {
            if (!this.id) return;

            var region = JS9.GetRegions(this.id)[0],
                l = parseInt($(e.target).val(), 10),
                r = parseInt($('#image-sub_image_right').val(), 10);

            var width = Math.abs(l-1 - r);
            JS9.ChangeRegions(this.id, {
                x: l + (width/2),
                y: region.y,
                width: width,
                height: region.height
            });
        }.bind(this));
        $('#image-sub_image_right').change(function(e) {
            if (!this.id) return;

            var region = JS9.GetRegions(this.id)[0],
                l = parseInt($('#image-sub_image_left').val(), 10),
                r = parseInt($(e.target).val(), 10);

            var width = Math.abs(l-1 - r);
            JS9.ChangeRegions(this.id, {
                x: r - (width/2),
                y: region.y,
                width: width,
                height: region.height
            });
        }.bind(this));

        $('#image-sub_image_bottom').change(function(e) {
            if (!this.id) return;

            var region = JS9.GetRegions(this.id)[0],
                b = parseInt($(e.target).val(), 10),
                t = parseInt($('#image-sub_image_top').val(), 10);

            var height = Math.abs(b-1 - t);
            JS9.ChangeRegions(this.id, {
                x: region.x,
                y: b + (height/2),
                width: region.width,
                height: height
            });
        }.bind(this));
        $('#image-sub_image_top').change(function(e) {
            if (!this.id) return;

            var region = JS9.GetRegions(this.id)[0],
                b = parseInt($('#image-sub_image_bottom').val(), 10),
                t = parseInt($(e.target).val(), 10);

            var height = Math.abs(b-1 - t);
            JS9.ChangeRegions(this.id, {
                x: region.x,
                y: t - (height/2),
                width: region.width,
                height: height
            });
        }.bind(this));

        $('#subimage-select').click(_.bind(function(e) {
            if (SHOC.camera.model.get('acquiring')) return;

            if (!this.id) {
                $(e.target).css('color', 'red');
                this.id = JS9.AddRegions('box', {
                    tags: ['SHOCsubimage'],
                    color: 'red',
                    hasRotatingPoint: false,
                    selectionLineWidth: 1,
                    strokeWidth: 1,
                    strokeDashArray: [1, 2]
                });

                // Change to custom sub-image option.
                $('#image-sub_image').val(5);

                onRegionsChange.call(
                    this, this.display.image, JS9.GetRegions(this.id)[0]);
            } else {
                $(e.target).css('color', '#fff');
                JS9.RemoveRegions(this.id);
                this.id = null;
            }
        }, this));
    }

    function onRegionsChange(image, region) {
        if (this.id != region.id || region.mode == 'remove') {
            if (region.mode == 'remove') {
                $('#subimage-select').css('color', '#fff');
                this.id = null;
            }
            return;
        }

        // The x and y coordinates are at the center of the box so we need to
        // subtract half of the width and height to get the coordinates of the
        // bottom left corner.
        var l = Math.floor(region.x - (region.width/2)),
            b = Math.floor(region.y - (region.height/2));

        $('#image-sub_image_left').val(Math.max(l, 1));
        $('#image-sub_image_right').val(
            Math.min(Math.floor(l + region.width), image.raw.width));
        $('#image-sub_image_bottom').val(Math.max(b, 1));
        $('#image-sub_image_top').val(
            Math.min(Math.floor(b + region.height), image.raw.height));

        // Manually trigger the change event so that the new settings are sent
        // to the camera.
        $('#image-sub_image').trigger('change');
    }

    JS9.RegisterPlugin('SHOC', 'subimage', init, {
        onregionschange: onRegionsChange,
        winDims: [0, 0]
    });

})();
