SHOC.GPS = {};

SHOC.GPS.Model = Backbone.Model.extend(_.extend({}, SHOC.Mixins.Poller, {
    url: '/instruments/gps',

    defaults: {
        id: 1 // There is only one GPS 'model' so we hardcode the ID property.
    }
}));

SHOC.GPS.FormView = Backbone.View.extend({
    el: '#gps',

    events: {
        'click .btn-save': 'save',
        'click .btn-stop': 'stop',
        'click .btn-quick-start': 'quickStart',
        'change #mode': 'changeMode'
    },

    save: function(e) {
        var $button = $(e.target).button('loading');

        // Remove any validation error messages.
        this.$('.has-error .help-block').each(function() {
            $(this).text($(this).data('original-text'));
        });
        this.$('.has-error').removeClass('has-error');

        this.model.patch({
            pop: {
                mode: this.$('#mode').val(),
                start_date: this.$('#start_date').val(),
                start_time: this.$('#start_time').val(),
                pulse_width: this.$('#pulse_width').val(),
                repeat_interval: this.$('#repeat_interval').val()
            }
        }).always(function() {
            setTimeout(function() {
                $button.button('reset')
            }, 500);
        });

        return false;
    },

    stop: function(e) {
        var $button = $(e.target).button('loading');

        e.preventDefault();

        this.model.patch({
            pop: {
                mode: '0',
                start_date: '',
                start_time: '',
                pulse_width: '0',
                repeat_interval: ''
            }
        }).success(function(model, response, options) {
            $('#mode').val(0);
            $('#repeat_interval').attr('disabled', true);
        }).always(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });

        return false;
    },

    quickStart: function(e) {
        var d = new Date(Date.now() + 30*1000);

        $('#start_time').val(d.getHours().toString().padStart(2, '0') +
            ':' + d.getMinutes().toString().padStart(2, '0') +
            ':' + d.getSeconds().toString().padStart(2, '0'));
    },

    changeMode: function(e) {
        // Disable repeat interval when the mode isn't set to "Repeat".
        if (e.target.value == 2) {
            $('#repeat_interval').attr('disabled', false);
        } else {
            $('#repeat_interval').attr('disabled', true);
        }
    },

    initialize: function(gps_status) {
        this.model = new SHOC.GPS.Model(gps_status);
        this.model.on('error', function(model, response, options) {
            // If options.retry is set and true we can ignore the error because
            // the request was initiated by a collection or model that uses
            // SHOC.Mixins.Poller and retries will be handled there.
            if (!options.retry) {
                var message = 'An unknown error occurred.';
                if (response.status === 0) {
                    message = 'The instrument is not responding.';
                } else if (response.status === 400) {
                    // Display validation errors on the form.
                    message = 'Please correct the errors below and try again.';
                    $(Object.keys(response.responseJSON.errors)).each(function(i, k) {
                        $('#' + k).closest('.form-group').addClass('has-error').find(
                            '.help-block').text(response.responseJSON.errors[k]);
                    });
                } else if (response.responseJSON.error) {
                    // When in production mode we display a friendlier-looking
                    // error message to the user.
                    message = response.responseJSON.error;
                }
                SHOC.alert.render('alert-danger', message);
            }
        });

        $('select[name="mode"]').val(this.model.get('pop').mode).trigger('change');
        $('select[name="pulse_width"]').val(this.model.get('pop').pulse_width);
        $('input[name="start_date"]').val(this.model.get('pop').start_date);
        $('input[name="start_time"]').val(this.model.get('pop').start_time);
        $('input[name="repeat_interval"]').val(this.model.get('pop').repeat_interval);

        this.$('.input-group.date').datepicker({
            autoclose: true,
            orientation: 'top left'
        });

        this.model.on('change', function(model) {
            var gps = model.attributes;

            $('.local-time').text(gps.local_datetime);
            $('.universal-time').text(gps.universal_datetime);

            if (gps.timing_status === 0) {
                $('#timing-status .info').text('Not Valid');
                $('#timing-status .state').attr('class', 'state bg-danger');
            } else {
                $('#timing-status .info').text('Valid');
                $('#timing-status .state').attr('class', 'state bg-success');
            }

            switch (gps.pop.state) {
                case 0:
                    $('#pop-status .info').text('Off');
                    $('#pop-status .state').attr('class', 'state bg-primary');
                    break;
                case 1:
                    $('#pop-status .info').text('Pending');
                    $('#pop-status .state').attr('class', 'state bg-warning');
                    break;
                case 2:
                    $('#pop-status .info').text('Pending');
                    $('#pop-status .state').attr('class', 'state bg-warning');
                    break;
                case 3:
                    $('#pop-status .info').text('Activated');
                    $('#pop-status .state').attr('class', 'state bg-success');
                    break;
                case 4:
                    $('#pop-status .info').text('Fault');
                    $('#pop-status .state').attr('class', 'state bg-danger');
                    break;
            }

            if (gps.oscillator_mode !== 4) {
                $('#reference-status .info').text('Off');
                $('#reference-status .state').attr('class', 'state bg-danger');
            } else {
                $('#reference-status .info').text('Ready');
                $('#reference-status .state').attr('class', 'state bg-success');
            }

            if (gps.antenna === 1) {
                $('#antenna-status .info').text('Fault');
                $('#antenna-status .state').attr('class', 'state bg-danger');
            } else {
                $('#antenna-status .info').text('Good');
                $('#antenna-status .state').attr('class', 'state bg-success');
            }

            $('.status-position-latitude').html(gps.position.latitude);
            $('.status-position-longitude').html(gps.position.longitude);
            $('.status-user-time-bias').text(gps.user_time_bias + ' ns');
        });

        new SHOC.GPS.SatelliteView({
            model: this.model
        });

        this.model.poll(5);
    }
});

SHOC.GPS.SatelliteView = Backbone.View.extend({
    height: 150,

    initialize: function() {
        this.model.on('change', function() {
            this.redraw();
        }, this);

        this.scale = d3.scale.linear()
            .domain([0, 9])
            .range([0, this.height - 20]);
    },

    redraw: function() {
        var data = this.model.get('satellites'),
            keys = d3.keys(data),
            svg = d3.select('#viz');

        svg.selectAll('rect.sq')
            .data(keys.map(function(k) {
                return data[k][2];
            }))
            .transition()
            .duration(500)
            .attr('y', _.bind(function(d) {
                return (isNaN(d) || d == 0 ? this.height - 1 : this.height - this.scale(d));
            }, this))
            .attr('height', _.bind(function(d) {
                return (isNaN(d) || d == 0 ? 1 : this.scale(d));
            }, this));

        svg.selectAll('text.prn')
            .data(keys.map(function(k) {
                return data[k][0];
            }))
            .transition()
            .text(function(d) { return d; });

        svg.selectAll('text.sq')
            .data(keys.map(function(k) {
                return data[k][2];
            }))
            .transition()
            .text(function(d) { return d; });

        svg.selectAll('rect.status')
            .data(d3.keys(data).map(function(k) {
                return data[k][3];
            }))
            .transition()
            .attr('class', function(d) {
                return d == 1 ? 'status active' : 'status';
            });
    }
});
