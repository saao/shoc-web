SHOC.Filterwheel = {};

SHOC.Filterwheel.Collection = Backbone.Collection.extend(_.extend({}, SHOC.Mixins.Poller, {
    url: '/instruments/wheels/',

    model: Backbone.Model.extend({
        idAttribute: 'name',

        defaults: {
            required_position: 1,
            initialized: false
        }
    })
}));

SHOC.Filterwheel.View = Backbone.View.extend({
    initialize: function(wheels) {
        this.collection = new SHOC.Filterwheel.Collection(wheels);
        this.collection.each(function(wheel) {
            f = new SHOC.Filterwheel.FormView({
                el: '#' + wheel.get('name'),
                model: wheel
            });
        });
        this.collection.on('error', function(collection, response, options) {
            // If options.retry is set and true we can ignore the error because
            // the request was initiated by a collection or model that uses
            // SHOC.Mixins.Poller and retries will be handled there.
            if (!options.retry) {
                var message = 'An unknown error occurred.';
                if (response.status === 0) {
                    message = 'The instrument is not responding.';
                } else if (response.responseJSON.error) {
                    // When in production mode we display a friendlier-looking
                    // error message to the user.
                    message = response.responseJSON.error;
                }
                SHOC.alert.render('alert-danger', message);
            }
        });
        this.collection.poll(5);
    }
});

// The reset is done from a separate view because we're using a Bootstrap
// modal window which is not contained within the main form view.
SHOC.Filterwheel.ResetFormView = Backbone.View.extend({
    el: '#reset-confirmation',

    events: {
        'click .btn-reset': 'reset',
    },

    reset: function(e) {
        var $button = $(e.target).button('loading'),
            $el = this.$el;

        this.model.patch({
            reset: true
        }).always(function() {
            $el.modal('hide');
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    }
});

SHOC.Filterwheel.FormView = Backbone.View.extend({
    events: {
        'click .btn-initialize': 'init',
        'click .btn-move': 'move',
        'click .advanced-toggle': 'toggleAdvancedOptions',
        'change [name=required-position]': 'updateRequiredPosition'
    },

    init: function(e) {
        var $button = $(e.target).button('loading');

        this.model.patch({
            initialized: true
        }).always(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    move: function(e) {
        var $button = $(e.target).button('loading');

        this.model.patch({
            required_position: this.model.get('required_position')
        }).always(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    toggleAdvancedOptions: function(e) {
        var $options = this.$('.advanced-options');

        $options.toggle();
        if ($options.is(':visible')) {
            e.target.innerHTML = 'Less&hellip;';
        } else {
            e.target.innerHTML = 'More&hellip;';
        }

        e.preventDefault();
    },

    updateRequiredPosition: function(e) {
        this.model.set({
            required_position: parseInt(e.target.value, 10)
        });
    },

    initialize: function() {
        this.$('.spinedit').spinedit({
            minimum: 1,
            maximum: this.model.get('filters').length || 8,
            value: 0,
            step: 1
        });

        this.model.on('sync', function() {
            var wheel = this.model.attributes;

            if (wheel.initialized) {
                this.$('.status-initialized').removeClass(
                    'label-default').addClass('label-success');
            } else {
                this.$('.status-initialized').removeClass(
                    'label-success').addClass('label-default');
            }
        }, this);

        this.model.on('change', function() {
            var wheel = this.model.attributes;

            if (wheel.initialized) {
                this.$('.status-initialized').removeClass(
                    'label-default').addClass('label-success');
            } else {
                this.$('.status-initialized').removeClass(
                    'label-success').addClass('label-default');
            }
            if (wheel.at_reference) {
                this.$('.status-reference').removeClass(
                    'label-default').addClass('label-success');
            } else {
                this.$('.status-reference').removeClass(
                    'label-success').addClass('label-default');
            }
            if (wheel.centered) {
                this.$('.status-centered').removeClass(
                    'label-default').addClass('label-success');
            } else {
                this.$('.status-centered').removeClass(
                    'label-success').addClass('label-default');
            }
            if (wheel.moving) {
                this.$('.status-moving').removeClass(
                    'label-default').addClass('label-danger');
            } else {
                this.$('.status-moving').removeClass(
                    'label-danger').addClass('label-default');
            }

          // Make sure that the wheel ID is a four bit binary string.
          console.log("Converting " + wheel.wheel_id + " to four bit binary string");
            var wheel_id = ('00' + (wheel.wheel_id).toString(3)).slice(-4);

            this.$('.wheel-id').text(wheel_id);
            this.$('[name="position"]').val(wheel.position);

            if (wheel.filters && typeof wheel.filters !== 'undefined') {
                var filter = wheel.filters[wheel.position - 1];
                if (typeof filter !== 'undefined') {
                    this.$('.position-name').text(filter[0]);
                    this.$('.position-colour').attr(
                        'style', 'background-color: #' + filter[1] + ';');
                } else {
                    this.$('.position-name').text('');
                    this.$('.position-colour').attr(
                        'style', 'background-color: none;');
                }

                var filter = wheel.filters[wheel.required_position - 1];
                if (typeof filter !== 'undefined') {
                    this.$('.required-position-name').text(filter[0]);
                    this.$('.required-position-colour').attr(
                        'style', 'background-color: #' + filter[1] + ';');
                }
            }
        }, this);

        // Manually trigger a model change to initialize the view state.
        this.model.trigger('change');
    }
});

jQuery(function($) {
    // Sets the value of the 'wheel' input in the reset confirmation window by
    // looking up the value of the 'wheel' input in the parent form of the
    // reset button that opened the confirmation window.
    $('body').on('show.bs.modal', '#reset-confirmation', function(e) {
        var wheel = $(e.relatedTarget).closest('form').find('[name="name"]').val();

        // Instantiate the view so we can handle the reset event.
        new SHOC.Filterwheel.ResetFormView({
            model: SHOC.filterwheel.collection.get(wheel)
        });
    });
});
