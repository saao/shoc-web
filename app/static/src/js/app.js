var SHOC = {};

Backbone.Model.prototype.patch = function(data) {
    SHOC.alert.hide();
    return this.save(data, {patch: true, wait: true});
};

SHOC.Alert = Backbone.View.extend({
    el: '#alert',

    events: {
        'click .close': 'hide'
    },

    hide: function() {
        this.$el.addClass('hidden');
    },

    render: function(type, message) {
        this.$el.removeClass('hidden alert-danger alert-success alert-warning alert-info')
            .addClass(type)
            .find('.alert-body')
            .html(message);
    }
});

SHOC.Mixins = {
    Poller: {
        poll: function(countdown) {
            var fetch = _.bind(this.poll, this);

            this.fetch({
                retry: true,
                timeout: 2000,
                error: function(model, response, options) {
                    if (countdown > 0) {
                        console.debug('Retrying', countdown);

                        setTimeout(_.partial(fetch, --countdown), 1000);
                    } else {
                        $('span.status').fadeOut();
                        SHOC.alert.render('alert-danger',
                            'Connection lost: <a href="#" onclick="window.location.reload(); return false;" class="alert-link">Reload the page</a> to try again.');
                    }
                },
                success: function() {
                    setTimeout(_.partial(fetch, 5), 1000);
                }
            });
        }
    }
};

jQuery(function($) {
    SHOC.alert = new SHOC.Alert();

    $('body').popover({
        selector: '.btn, .info, [data-toggle=popover]',
        html: true,
        animation: false,
        container: 'body',
        placement: function(popover, el) {
            var placement = $(el).data('placement');
            if (typeof placement !== 'undefined') {
                return placement;
            } else if (window.innerWidth < 1000) {
                return 'auto';
            } else {
                return 'right';
            }
        },
        trigger: 'hover'
    });

    // Global error handler for ajax requests.
    $(document).ajaxError(function(e, xhr, options) {
        if (xhr.responseJSON && xhr.responseJSON.debug) {
            var message = '<div style="white-space: pre-wrap;">' +
                xhr.responseJSON.debug.replace(/\n/g, "\n") + '</div>';
            SHOC.alert.render('alert-danger', message);
        }
    });

    // https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
    if (!String.prototype.padStart) {
        String.prototype.padStart = function padStart(targetLength, padString) {
            targetLength = targetLength >> 0; //truncate if number, or convert non-number to 0;
            padString = String(typeof padString !== 'undefined' ? padString : ' ');
            if (this.length >= targetLength) {
                return String(this);
            } else {
                targetLength = targetLength - this.length;
                if (targetLength > padString.length) {
                    padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
                }
                return padString.slice(0, targetLength) + String(this);
            }
        };
    }
});
