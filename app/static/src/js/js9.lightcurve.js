/* Plots a light curve as the sum of counts in a selected region. */

(function() {
    "use strict";

    var imexam = require('./imexam');

    function queue(size) {
        var queue = [];

        return {
            push: function(item) {
                if (queue.length >= size) {
                    queue = queue.slice(1);
                }

                queue.push(item);

                return queue;
            }
        };
    }

    function init() {
        var $div = $(this.div);

        this.q = queue(550);

        this.plot = $.plot(this.div, [], {
            grid: {
                margin: {
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0
                },
                borderWidth: 1,
                minBorderMargin: 0,
                markings: function(axes) {
                    if (!axes.yaxis.datamin && !axes.yaxis.datamax) return [];

                    $div.find('.lightcurve-min').html('summed min: ' + Math.floor(axes.yaxis.datamin))
                        .css({top: axes.yaxis.p2c(axes.yaxis.datamin) - 15});
                    $div.find('.lightcurve-max').html('summed max: ' + Math.floor(axes.yaxis.datamax))
                        .css({top: axes.yaxis.p2c(axes.yaxis.datamax)});

                    return [
                        {yaxis: {
                            from: axes.yaxis.datamin,
                            to: axes.yaxis.datamin
                        }, color: 'rgba(0, 0, 128, 0.8)'},
                        {yaxis: {
                            from: axes.yaxis.datamax,
                            to: axes.yaxis.datamax
                        }, color: 'rgba(128, 0, 0, 0.8)'}
                    ];
                }
            },
            xaxis: {
                show: false
            },
            yaxis: {
                show: false
            }
        });

        $div.append('<div class="lightcurve-min" style="position: absolute; left: 10px; font-size: 10px; font-weight: bold;"></div><div class="lightcurve-max" style="position: absolute; left: 10px; font-size: 10px; font-weight: bold;"></div>');
    }

    function onImageDisplay(image) {
        var region = JS9.GetRegions('selected')[0];

        if (!region) return;

        var regionData = imexam.getRegionData(image, region);
        var data = imexam.ndops.assign(imexam.ndops.zeros(regionData.shape), regionData);
        var centroid = imexam.ndops.centroid(data, imexam.ndops.qcenter(data));

        var curve = this.q.push(centroid.sum);
        var data = curve.map(function(value, index) {
            return [index, value];
        });
        this.plot.setData([{
            data: data,
            color: '#428bca'
        }]);

        var that = this;
        requestAnimationFrame(function() {
            that.plot.setupGrid();
            that.plot.draw();
        });
    }

    JS9.RegisterPlugin('SHOC', 'lightcurve-totcounts', init, {
        onimagedisplay: onImageDisplay,
        winDims: [0, 0]
    });

})();
