SHOC.Camera = {};

function Countdown(n) {
    var $el = $('#countdown'), timeout, instance;

    function _countdown(n) {
        if (SHOC.camera.model.get('acquiring') && n > 0) {
            $el.css('display', 'block').text(parseInt(n));
            n -= 1;
            timeout = setTimeout(function() {
                _countdown(n);
            }, 1000);
        } else {
            $el.hide();
        }
    }

    if (typeof instance == 'undefined') {
        instance = {
            start: function() {
                clearTimeout(timeout);
                _countdown(n);
                return this;
            }
        };
    }

    return instance;
}

SHOC.Camera.Model = Backbone.Model.extend(_.extend({}, SHOC.Mixins.Poller, {
    url: '/instruments/camera',

    defaults: {
        id: 1, // There is only one camera 'model' so we hardcode the ID property.
        initialized: false,
        acquiring: false,
        preview: false,

        image: {
            // The default sub image size is the full size of the CCD.
            sub_image_left: 0,
            sub_image_right: 1024,
            sub_image_top: 0,
            sub_image_bottom: 1024,

            // The default binning is 1x1.
            binning_horizontal: 1,
            binning_vertical: 1
        }
    }
}));

SHOC.Camera.StopView = Backbone.View.extend({
    el: '#confirmation',

    events: {
        'click .btn-off': 'stop'
    },

    stop: function(e) {
        var $button = $(e.target).button('loading'),
            $el = this.$el;

        this.model.patch({
            initialized: false
        }).fail(function() {
            $el.modal('hide');
        }).always(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    initialize: function() {
        this.model = new SHOC.Camera.Model();
    }
});

SHOC.Camera.StartView = Backbone.View.extend({
    el: '#camera',

    events: {
        'click .btn-on': 'start'
    },

    start: function(e) {
        var $button = $(e.target).button('loading');

        this.model.patch({
            initialized: true
        }).success(function() {
            window.location.reload();
        }).fail(function(response) {
            var message = 'An unkown error occured.';
            if (response.status == 0) {
                message = 'The instrument is not responding.';
            } else if (response.responseJSON.error) {
                message = response.responseJSON.error;
            }
            SHOC.alert.render('alert-danger', message);

            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    initialize: function() {
        this.model = new SHOC.Camera.Model();
    }
});

SHOC.Camera.FormView = Backbone.View.extend({
    el: '#camera',

    events: {
        'click .btn-preview': 'preview',
        'click .btn-start': 'start',
        'click .btn-start-repeat': 'start',
        'click .btn-stop': 'stop',

        'click .btn-cooler-on': 'coolerOn',
        'click .btn-cooler-off': 'coolerOff',

        'change [name=target_temperature]': 'changeTemperature',

        'change [name=trigger_mode]': 'changeTriggerMode',
        'change [name=exposure_time]': 'changeExposureTime',

        'change [name=kinetic_series_length]': 'changeKineticSeriesLength',

        'change [name^=image-]': 'changeImage',

        'change [name=start_index]': 'changeStartIndex',

        'change [name=horizontal_shift_speed]': 'changeHorizontalShiftSpeed',
        'change [name=preamp_gain]': 'changePreampGain',
        'change [name=output_amplifier]': 'changeOutputAmplifier',
        'change [name=em_gain]': 'changeEMGain',

        'change [name^=observation-]': 'changeObservation',

        'change [name=flip_x]': 'changeImageFlip',
        'change [name=flip_y]': 'changeImageFlip',

        'change [name=script_iterations]': 'changeScriptIterations'
    },

    preview: function(e) {
        var $button = $(e.target).button('loading');

        this.model.patch({
            preview: true
        }).fail(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    start: function(e) {
        console.log("Setting start on");
        var $button = $(e.target).button('loading');

        if ($(e.target).hasClass('btn-start-repeat')) {
            $('#repeat').modal('hide');
        } else {
            console.log("Sending patch");
            this.model.patch({
                script_iterations: 1
            });
        }

    //     return;
    // },

        this.model.patch({
            acquiring: true
        }).done(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    stop: function(e) {
        var $button = $(e.target).button('loading');

        this.model.patch({
            acquiring: false
        }).fail(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    coolerOn: function(e) {
        console.log("Setting cooler on");
//        var $button = $(e.target).button('loading');
        var $button = $(e.target);

        this.model.patch({
            cooler: true
        }).fail(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    coolerOff: function(e) {
        var $button = $(e.target);

        this.model.patch({
            cooler: false
        }).fail(function() {
            setTimeout(function() {
                $button.button('reset');
            }, 500);
        });
    },

    changeTemperature: function(e) {
        var temperature = parseInt(e.target.value, 10);

        if (isNaN(temperature)) {
            e.target.value = this.model.get('target_temperature');
        } else {
            this.model.patch({
                temperature: temperature
            });
        }
    },

    changeTriggerMode: function(e) {
        var trigger_mode = parseInt(e.target.value, 10);

        // Disable exposure time when using external triggering.
        if (trigger_mode == 1) {
            $('[name=exposure_time]').attr('disabled', true);

            // The labels for exposure and kinetic cycle time are different
            // when using an external trigger.
            $('label[for=exposure_time]').text('Delay');
            $('label[for=kinetic_cycle_time]').text('Min Cycle Time');
        } else {
            $('[name=exposure_time]').attr('disabled', false);

            $('label[for=exposure_time]').text('Exposure Time');
            $('label[for=kinetic_cycle_time]').text('Cycle Time');
        }

        this.model.patch({trigger_mode: trigger_mode});
    },

    changeExposureTime: function(e) {
        var exposure_time = parseFloat(e.target.value, 10);

        if (isNaN(exposure_time)) {
            e.target.value = this.model.get('exposure_time');
        } else {
            this.model.patch({
                exposure_time: exposure_time
            }).done(function(model) {
                // The user might try to set an exposure time that is less than the
                // allowed value in which case it will revert to that value so we
                // need to update the user interface.
                if (model.exposure_time != exposure_time) {
                    e.target.value = model.exposure_time;
                }
            });
        }
    },

    changeKineticSeriesLength: function(e) {
        var kinetic_series_length = parseInt(e.target.value, 10);

        if(isNaN(kinetic_series_length)) {
            e.target.value = this.model.get('kinetic_series_length');
        } else {
            this.model.patch({
                kinetic_series_length: kinetic_series_length
            });
        }
    },

    changeImage: function(e) {
        var sub_image = parseInt($('#image-sub_image').val(), 10),
            binning = parseInt($('#image-binning').val(), 10);

        // Show or hide the custom fields based on the sub image and binning
        // options.
        if (sub_image == 5 || binning == 5) {
            $('#image').removeClass('hide');
        } else {
            $('#image').addClass('hide');
        }

        $('[name^=image-sub_image_]').attr('disabled', (sub_image != 5));
        $('[name^=image-binning_]').attr('disabled', (binning != 5));

        if (sub_image != 5) {
            this.changeSubImage(sub_image);
        }
        this.changeBinning(binning);

        var l = parseInt($('#image-sub_image_left').val(), 10);
        if (isNaN(l) || l <= 0 || l >= 1024) {
            $('#image-sub_image_left').val(this.model.get('image').sub_image_left);
            return false;
        }
        var r = parseInt($('#image-sub_image_right').val(), 10);
        if (isNaN(r) || r > 1024 || r <= 1) {
            $('#image-sub_image_right').val(this.model.get('image').sub_image_right);
            return false;
        }
        var h = parseInt($('#image-binning_horizontal').val(), 10);
        if (isNaN(h) || h < 1) {
            $('#image-binning_horizontal').val(this.model.get('image').binning_horizontal);
            return false;
        }

        var w = Math.abs((l-1) - r);
        var n = r - (w % h);

        if (n == 0) {
            n = Math.ceil(w/h)*h;
        } else if (n <= l) {
            n = l + (Math.ceil(w/h)*h)-1;
        }

        // If the new value ends up being more than the allowed maximum, reset
        // both left and right frame values.
        if (n > 1024) {
            $('#image-sub_image_left').val(this.model.get('image').sub_image_left);
            $('#image-sub_image_right').val(this.model.get('image').sub_image_right);
            return false;
        }

        $('#image-sub_image_right').val(n);

        var b = parseInt($('#image-sub_image_bottom').val(), 10);
        if (isNaN(b) || b <= 0 || b >= 1024) {
            $('#image-sub_image_bottom').val(this.model.get('image').sub_image_bottom);
            return false;
        }
        var t = parseInt($('#image-sub_image_top').val(), 10);
        if (isNaN(t) || t > 1024 || t <= 1) {
            $('#image-sub_image_top').val(this.model.get('image').sub_image_top);
            return false;
        }
        var v = parseInt($('#image-binning_vertical').val(), 10);
        if (isNaN(v) || v < 1) {
            $('#image-binning_vertical').val(this.model.get('image').binning_vertical);
            return false;
        }

        var h = Math.abs((b-1) - t);
        var n = t - (h % v);

        if (n == 0) {
            n = Math.ceil(h/v)*v;
        } else if (n <= b) {
            n = b + (Math.ceil(h/v)*v)-1;
        }

        // If the new value ends up being more than the allowed maximum, reset
        // both bottom and top frame values.
        if (n > 1024) {
            $('#image-sub_image_bottom').val(this.model.get('image').sub_image_bottom);
            $('#image-sub_image_top').val(this.model.get('image').sub_image_top);
            return false;
        }

        $('#image-sub_image_top').val(n);

        this.model.patch({
            image: {
                sub_image_left: parseInt($('#image-sub_image_left').val(), 10),
                sub_image_bottom: parseInt($('#image-sub_image_bottom').val(), 10),
                sub_image_right: parseInt($('#image-sub_image_right').val(), 10),
                sub_image_top: parseInt($('#image-sub_image_top').val(), 10),

                binning_horizontal: parseInt($('#image-binning_horizontal').val(), 10),
                binning_vertical: parseInt($('#image-binning_vertical').val(), 10)
            }
        });
    },

    changeSubImage: function(sub_image) {
        var $bottom_left = $('#image-sub_image_bottom, #image-sub_image_left'),
            $top_right = $('#image-sub_image_top, #image-sub_image_right');

        var regions = JS9.GetRegions();
        for (var i = 0; i < regions.length; i++) {
            var region = regions[i];
            for (var j = 0; j < region.tags.length; j++) {
                if (region.tags[j] == 'SHOCsubimage') {
                    JS9.RemoveRegions(region.id);
                }
            }
        }

        switch(sub_image) {
            case 0: // 1024x1024
                $bottom_left.val(1);
                $top_right.val(1024);
                break;

            case 1: // 512x512
                $bottom_left.val(1);
                $top_right.val(512);
                break;

            case 2: // 256x256
                $bottom_left.val(1);
                $top_right.val(256);
                break;

            case 3: // 128x128
                $bottom_left.val(1);
                $top_right.val(128);
                break;

            case 4: // 64x64
                $bottom_left.val(1);
                $top_right.val(64);
                break;
        }
    },

    changeBinning: function(binning) {
        var $fields = this.$('#image-binning_horizontal, #image-binning_vertical');

        switch(binning) {
            case 0: // 1x1
                $fields.val(1);
                break;

            case 1: // 2x2
                $fields.val(2);
                break;

            case 2: // 4x4
                $fields.val(4);
                break;

            case 3: // 8x8
                $fields.val(8);
                break;

            case 4: // 16x16
                $fields.val(16);
                break;
        }
    },

    changeStartIndex: function(e) {
        var new_index = parseInt(e.target.value, 10),
            old_index = this.model.get('start_index'),
            confirmed = false;

        if (isNaN(new_index)) {
            e.target.value = old_index;
        } else {
            if (new_index < old_index) {
                var $modal = $('#start-index-confirmation').modal('show');
                $modal.on('click', '.btn-set-start-index', _.bind(function() {
                    confirmed = true;
                    this.model.patch({
                        start_index: new_index
                    });
                }, this)).one('hidden.bs.modal', function() {
                    // Revert to the old value when the user closes the
                    // confirmation window.
                    if (!confirmed) {
                        e.target.value = old_index;
                    }
                    $(this).off('click', '.btn-set-start-index');
                });
            } else {
                this.model.patch({
                    start_index: new_index
                });
            }
        }
    },

    changeHorizontalShiftSpeed: function(e) {
        this.model.patch({
            horizontal_shift_speed: e.target.value
        });
    },

    changePreampGain: function(e) {
        this.model.patch({
            preamp_gain: e.target.value
        });
    },

    changeOutputAmplifier: function(e) {
        var output_amplifier = e.target.value;

        // Prompt the user so that they are aware of the dangers when operating
        // in EM mode.
        if (output_amplifier == 0) {
            $('#output_amplifier-1').prop('checked', true);
            var $modal = $('#em-confirmation').modal('show');
            $modal.on('click', '.btn-enable-em', _.bind(function() {
                $('#output_amplifier-0').prop('checked', true);
                this.model.patch({
                    output_amplifier: output_amplifier
                }).done(function() {
                    $('#em_gain').attr('disabled', false);
                });
            }, this)).one('hidden.bs.modal', function(e) {
                $(this).off('click', '.btn-enable-em');
            });
        } else {
            this.model.patch({
                output_amplifier: output_amplifier
            }).done(function() {
                $('#em_gain').attr('disabled', true);
            });
        }
    },

    changeEMGain: function(e) {
        var em_gain = parseInt(e.target.value, 10);

        if (isNaN(em_gain)) {
            e.target.value = this.model.get('em_gain');
        } else {
            this.model.patch({
                em_gain: e.target.value
            });
        }
    },

    changeObservation: function(e) {
        this.model.patch({
            'observation': {
                'observer': $('#observation-observer').val(),
                'obstype': $('#observation-obstype').val(),
                'object': $('#observation-object').val(),
                'objra': $('#observation-objra').val(),
                'objdec': $('#observation-objdec').val(),
                'objepoch': $('#observation-objepoch').val(),
                'objequin': $('#observation-objequin').val()
            }
        });
    },

    changeImageFlip: function(e) {
        this.model.patch({
            'flip': {
                'x': parseInt($('[name=flip_x]:checked').val(), 10),
                'y': parseInt($('[name=flip_y]:checked').val(), 10)
            }
        });
    },

    changeScriptIterations: function(e) {
        var iterations = parseInt(e.target.value, 10);

        if (isNaN(iterations)) {
            e.target.value = this.model.get('script_iterations');
        } else {
            this.model.patch({
                'script_iterations': iterations
            });
        }
    },

    initialize: function() {
        this.model = new SHOC.Camera.Model();
        this.model.on('error', function(model, response, options) {
            // If options.retry is set and true we can ignore the error because
            // the request was initiated by a collection or model that uses
            // SHOC.Mixins.Poller and retries will be handled there.
            if (!options.retry) {
                var message = 'An unknown error occurred.';
                if (response.status == 0) {
                    message = 'The instrument is not responding.';
                } else if (response.status === 400) {
                    // Display validation errors on the form.
                    message = 'Please correct the errors below and try again.';
                    $(Object.keys(response.responseJSON.errors)).each(function(i, k) {
                        $('#' + k).closest('.form-group').addClass('has-error').find(
                            '.help-block').text(response.responseJSON.errors[k]);
                    });
                } else if (response.responseJSON.error) {
                    // When in production mode we display a friendlier-looking
                    // error message to the user.
                    message = response.responseJSON.error;
                }
                SHOC.alert.render('alert-danger', message);
            }
        });
        this.model.on('change:temperature', function(model, temperature) {
            this.$('.temperature-status').html(
                model.get('temperature') + '&deg; C');
        }, this);
        this.model.on('change:temperature_state', function(model, state) {
            switch(state) {
                case 20034: // cooler off
                    this.$('.temperature').removeClass(
                        'label-warning label-success').addClass('label-danger');
                    break;

                case 20037: // not reached
                case 20035: // not stable
                    this.$('.temperature').removeClass(
                        'label-danger label-success').addClass('label-warning');
                    break;

                case 20036: // stable
                    this.$('.temperature').removeClass(
                        'label-danger label-warning').addClass('label-success');
                    break;
            }
        }, this);
        this.model.on('change:acquiring', function(model, acquiring) {
            var acquisition_mode = model.get('acquisition_mode');

            $('fieldset').attr('disabled', acquiring);
            $('.progress-bar').width('0%').removeClass('progress-bar-success');

            if (acquiring) {

                if (model.get('trigger_mode') == 0
                        && model.get('exposure_time') >= 10) {
                    Countdown(model.get('exposure_time')).start();
                }

                // Remove the subimage selection region when we start acquiring.
                var regions = JS9.GetRegions() || [];
                for (var i = 0; i < regions.length; i++) {
                    var region = regions[i];
                    for (var j = 0; j < region.tags.length; j++) {
                        if (region.tags[j] == 'SHOCsubimage') {
                            JS9.RemoveRegions(region.id);
                        }
                    }
                }

                $('#mode, #mode .dropdown-toggle').addClass('disabled');
                $('.status').css('display', 'inline-block');
                $('.remove').hide();

                $('.btn-repeat').attr('disabled', true);

                switch(acquisition_mode) {
                    case 1: // Single scan
                    case 3: // Kinetic
                        $('.btn-preview').attr('disabled', true);
                        $('.btn-start').replaceWith(
                            '<button type="button" class="btn btn-default btn-block btn-stop">Stop</button>');

                        if (model.get('script')) {
                            var progress = model.get('script_progress');
                            window.location.hash = $('#script-lines table tr:nth-child(' + progress[0] + ')').attr('id');
                        }
                        break;

                    case 5: // Continuous
                        $('.btn-start').attr('disabled', true);
                        $('.btn-preview').replaceWith(
                            '<button type="button" class="btn btn-default btn-block btn-stop">Stop</button>');
                        break;
                }

                // Wait for the socket connection to open before sending anything.
                if (SHOC.camera.socket.readyState == WebSocket.CONNECTING) {
                    SHOC.camera.socket.onopen = function() {
                        SHOC.camera.socket.send('');
                    };
                } else {
                    SHOC.camera.socket.send('');
                }
            } else {
                if (acquisition_mode == 3 || acquisition_mode == 1) {
                    // Notify the user that the acquisition is complete.

                    if (localStorage.getItem('shoc:sound')) {
                        var audio = document.createElement('audio')
                        audio.src = localStorage.getItem('shoc:sound');
                        audio.play();
                    }
                    if (!('Notification' in window)) {
                        // Notifications aren't supported in this browser.
                    } else if (Notification.permission === 'granted') {
                        var notification = new Notification('Acquisition complete!');
                    } else if (Notification.permission !== 'denied') {
                        Notification.requestPermission(function (permission) {
                            if (permission === 'granted') {
                                var notification = new Notification('Acquisition complete!');
                            }
                        });
                    }
                }

                $('#mode, .dropdown-toggle').removeClass('disabled');
                $('.status').hide();
                $('.remove').show();

                $('.btn-repeat').attr('disabled', false);

                window.location.hash = '';
                window.history.replaceState({}, '', window.location.pathname);

                switch(acquisition_mode) {
                    case 1: // Single scan
                    case 3: // Kinetic
                        $('.btn-preview').attr('disabled', false);
                        $('.btn-stop').replaceWith(
                            '<button type="button" class="btn btn-default btn-block btn-start">Start</button>');

                        $('.progress-bar')
                            .width('100%')
                            .addClass('progress-bar-success')
                            .find('#progress').text(model.get('images_acquired'));
                        break;

                    case 5: // Continuous
                        $('.btn-start').attr('disabled', false);
                        $('.btn-stop').replaceWith(
                            '<button type="button" class="btn btn-default btn-block btn-preview">Preview</button>');
                        break;
                }
                // When we complete a run through a list of scripts we need to
                // upadte the form values to reflect that of the last line.
                if (model.get('script')) {
                    $('#exposure_time').val(model.get('exposure_time'));
                    $('#kinetic_series_length').val(model.get('kinetic_series_length'));
                    $('#image-sub_image').val(model.get('image').sub_image);
                    $('#image-binning').val(model.get('image').binning);
                    $('#horizontal_shift_speed').val(model.get('horizontal_shift_speed'));
                    $('#preamp_gain').val(model.get('preamp_gain'));
                }
            }
        });

        this.model.on('change:start_index', function(model, start_index) {
            $('#start_index').val(start_index);
        });
        this.model.on('change:save_name', function(model, save_name) {
            $('#save_name').text(save_name);
        });

        this.model.on('change:images_acquired', function(model) {
            var acquiring = model.get('acquiring'),
                acquisition_mode = model.get('acquisition_mode'),
                images_acquired = model.get('images_acquired'),
                kinetic_series_length = model.get('kinetic_series_length');

            // Start the countdown timer for internally triggered exposures
            // that last longer than 10 seconds.
            if (acquiring && model.get('trigger_mode') == 0
                    && model.get('exposure_time') >= 10 && images_acquired < kinetic_series_length) {
                Countdown(model.get('exposure_time')).start();
            }

            // Only update the progress bar when we're acquring a kinetic
            // series or a single scan.
            if (acquiring && (acquisition_mode == 3 || acquisition_mode == 1)) {
                var percentage_done = Math.floor(images_acquired/kinetic_series_length*100);

                if (model.get('exposure_time') >= 10 && images_acquired < kinetic_series_length) {
                    Countdown(model.get('exposure_time')).start();
                }

                $('.progress-bar')
                    .width(percentage_done + '%')
                    .find('#progress').text(images_acquired);
            }
        });

        this.model.on('change:kinetic_cycle_time', function(model, kinetic_cycle_time) {
            var kinetic_cycle_time_hertz = 0;
            if (kinetic_cycle_time > 0) {
                kinetic_cycle_time_hertz = (1 / kinetic_cycle_time)
                    .toPrecision(4) // Round to 3 decimal digits.
                    .replace(/\.?0+$/, ''); // Remove any trailing zeros.
            }

            $('#kinetic_cycle_time').val(kinetic_cycle_time);
            $('#kinetic_cycle_time_hertz').text(kinetic_cycle_time_hertz);
        });

        this.model.on('change:exposure_time', function(model, exposure_time) {
            $('#exposure_time').val(exposure_time);
        });

        this.model.on('change:horizontal_shift_speeds', function(model, speeds) {
            $el = $('#horizontal_shift_speed');

            if ($el.find('option').length == speeds.length) return;

            var options = '';
            speeds.forEach(function(name, value) {
                options += '<option value="' + value + '">' + name + '</option>';
            });

            $el.html(options).val(model.get('horizontal_shift_speed'));
        });

        this.model.on('change:preamp_gains', function(model, gains) {
            $el = $('#preamp_gain');

            if ($el.find('option').length == gains.length) return;

            var options = '';
            Object.keys(gains).forEach(function(key, index) {
                options += '<option value="' + key + '">' + gains[key] + '</option>';
            });

            $el.html(options).val(model.get('preamp_gain'));
        });

        this.model.on('change:preamp_gain', function(model, gain) {
            $('#preamp_gain').val(gain);
        });

        // Reload the interface when the camera is turned off. This will bring
        // up the initialisation page.
        this.model.on('change:initialized', function(model, initialized) {
            if (!initialized) {
                window.location.reload();
            }
        });

        this.model.on('change:script_progress', function(model, script) {
            if (!script) return;
            if (model.get('acquiring')) {
                if (model.get('script_iterations') > 1) {
                    $('.script-status').html(script[1] + ' of ' + model.get('script_iterations'));
                } else {
                    $('.script-status').html('');
                }
                window.location.hash = $('#script-lines table tr:nth-child(' + script[0] + ')').attr('id');
            }
        });

        // Start polling the camera state every second and attempt to retry
        // failed requests up to 5 times.
        this.model.poll(5);
    }
});

jQuery(function($) {
    $('body').on('show.bs.modal', '#confirmation', function(e) {
        new SHOC.Camera.StopView();
    });
});
