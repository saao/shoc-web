/* A modified version of the region stats plugin from JS9 */

(function() {
    "use strict";

    var imexam = require('./imexam');
    var template = '<table class="table" style="background-color: transparent;"> \
                    <tbody> \
                        <tr> \
                            <td align="right" style="width: 25%; border: none;">Center x</td> \
                            <td align="right" style="width: 25%; border: none;">{region.x%.0f}</td> \
                            <td align="right" style="width: 25%; border: none;">y</td> \
                            <td align="right" style="width: 25%; border: none;">{region.y%.0f}</td> \
                        </tr> \
                        <tr> \
                            <td align="right">width</td> \
                            <td align="right">{region.width%.1f} pixels</td> \
                            <td align="right">height</td> \
                            <td align="right">{region.height%.1f} pixels</td> \
                        </tr> \
                        <tr> \
                            <td align="right">min</td> \
                            <td align="right">{min%.0f} ADU</td> \
                            <td align="right">max</td> \
                            <td align="right">{max%.0f} ADU</td> \
                        </tr> \
                        <tr> \
                            <td align="right">totcounts</td> \
                            <td align="right">{centroid2.sum%.0f} ADU</td> \
                            <td align="right">bscounts</td> \
                            <td align="right">{centroid.sum%.0f} ADU</td> \
                        </tr> \
                        <tr> \
                            <td align="right">bkgrnd</td> \
                            <td align="right">{background.value%.0f}</td> \
                            <td align="right"></td> \
                            <td align="right"></td> \
                        </tr> \
                        <tr> \
                            <td align="right">Centroid x</td> \
                            <td align="right">{centroid.cenx%.0f}</td> \
                            <td align="right">y</td> \
                            <td align="right">{centroid.ceny%.0f}</td> \
                        </tr> \
                        <tr> \
                            <td align="right">FWHM</td> \
                            <td align="right">{centroid.fwhm%.0f}</td> \
                            <td align="right"></td> \
                            <td align="right"></td> \
                        </tr> \
                    </tbody> \
                </table>';

    function init() {
        return; // noop
    }

    function update(image, region) {
        // We don't get the active region when onimagedisplay is called.
        if (typeof region == 'undefined') {
            region = JS9.GetRegions('selected')[0];
        }

        // Even if it's an onimagedisplay call there could still be no active
        // region, so do nothing.
        if (!region) return;

        var section = imexam.reg2section(region);
        var regionData = imexam.getRegionData(image, region);
        var data = imexam.ndops.assign(imexam.ndops.zeros(regionData.shape), regionData);
        var data2 = imexam.ndops.assign(imexam.ndops.zeros(regionData.shape), regionData);
        var stat = {};

        stat.region = region;
        stat.min = imexam.ndops.minvalue(regionData);
        stat.max = imexam.ndops.maxvalue(regionData);
        stat.background = imexam.imops.backgr(regionData, 4);

        imexam.ndops.subs(data, regionData, stat.background.value);

        stat.qcenter = imexam.ndops.qcenter(data);
        stat.centroid = imexam.ndops.centroid(data, imexam.ndops.qcenter(data));
        stat.centroid2 = imexam.ndops.centroid(data2, imexam.ndops.qcenter(data2));

        stat.centroid.cenx += section[0][0];
        stat.centroid.ceny += section[1][0];

        requestAnimationFrame(function() {
            $('#region-stats').html(imexam.template(template, stat));
        });
    }

    JS9.RegisterPlugin('SHOC', 'regionstats', init, {
        onimagedisplay: update,
        onregionschange: update,
        winDims: [0, 0]
    });

})();
