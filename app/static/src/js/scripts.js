SHOC.Scripts = {};

SHOC.Scripts.Collection = Backbone.Collection.extend({
    url: '/instruments/camera/scripts/',

    comparator: 'name',

    model: Backbone.Model.extend({
        idAttribute: 'name',

        defaults: {
            lines: []
        },

        validate: function(attributes) {
            if (!attributes.name) {
                return 'Please enter a name for your new script.';
            }
        }
    })
});

SHOC.Scripts.ItemCollection = Backbone.Collection.extend({
    url: function() {
        return '/instruments/camera/scripts/' + SHOC.camera.model.get('script') + '/lines';
    },

    model: Backbone.Model.extend({
        validate: function(attributes) {
            var valid = attributes.exposure_time &&
                attributes.kinetic_series_length;
            if (!valid) {
                return 'Please enter valid values for all parameters.';
            }
        }
    })
});

SHOC.Scripts.CreateForm = Backbone.View.extend({
    el: '#add-script',

    events: {
        'click .btn-add': 'add'
    },

    add: function(e) {
        var $button = $(e.target).button('loading'),
            $el = this.$el,
            $input = $el.find('#script-name');

        this.$el.find('.modal-body').removeClass('has-error')
            .find('.help-block').text('');

        var script = this.collection.create({name: $input.val()}, {
            success: function(model) {
                setTimeout(function() {
                    $button.button('reset');
                    $el.modal('hide');
                    $input.val('');
                    model.trigger('selected', model);
                }, 500);
            },
            wait: true
        });
        if (script.validationError) {
            this.$el.find('.modal-body').addClass('has-error')
                .find('.help-block').text(script.validationError);
            $button.button('reset');
        }
    }
});

SHOC.Scripts.ItemForm = Backbone.View.extend({
    el: '#script-item-form',

    events: {
        'click .btn-add': 'add'
    },

    initialize: function() {
        this.$filterA = this.$el.find('[name=script_filter_A]');
        this.$filterB = this.$el.find('[name=script_filter_B]');
        this.$preampGain = this.$el.find('[name=script_preamp_gain]');
        this.$horizontalShiftSpeed = this.$el.find('[name=script_horizontal_shift_speed]');
        this.$exposureTime = this.$el.find('[name=script_exposure_time]');
        this.$kineticSeriesLength = this.$el.find('[name=script_kinetic_series_length]');
        this.$subImage = this.$el.find('[name=script_sub_image]');
        this.$binning = this.$el.find('[name=script_binning]');
    },

    add: function() {
        var item = this.collection.create({
            filters: {
                'A': parseInt(this.$filterA.val(), 10),
                'B': parseInt(this.$filterB.val(), 10)
            },
            preamp_gain: parseInt(this.$preampGain.val(), 10),
            horizontal_shift_speed: parseInt(this.$horizontalShiftSpeed.val(), 10),
            exposure_time: this.$exposureTime.val(),
            kinetic_series_length: this.$kineticSeriesLength.val(),
            sub_image: parseInt(this.$subImage.val(), 10),
            binning: parseInt(this.$binning.val(), 10)
        }, {
            success: function() {
                SHOC.alert.hide();
            },
            wait: true
        })
        if (item.validationError) {
            SHOC.alert.render('alert-danger', item.validationError);
        }
    }
});

SHOC.Scripts.ListView = Backbone.View.extend({
    el: '#scripts table tbody',

    events: {
        'click .remove': 'destroy'
    },

    template: _.template($('#script-list-item').html()),

    initialize: function() {
        this.collection.on('add', this.add, this);
    },

    add: function(model) {
        this.$el.append(this.template(model.toJSON()));
        SHOC.scripts.collection.fetch();
        $('.btn-start, .btn-repeat').removeClass('disabled');
    },

    destroy: function(e) {
        var row = $(e.target).closest('tr')[0];
        this.collection.get(row.id).destroy()
            .complete(function() {
                $(row).remove();
            });
        if (this.collection.length == 0)  {
            $('.btn-start, .btn-repeat').addClass('disabled');
        }

        e.preventDefault();
    },

    render: function() {
        this.$el.html('');
        this.collection.forEach(function(model) {
            this.$el.append(this.template(model.toJSON()));
        }.bind(this));
        return this;
    },

    remove: function() {
        this.$el.empty();
        this.undelegateEvents();
    }
});

SHOC.Scripts.MenuItemView = Backbone.View.extend({
    tagName: 'li',

    className: 'script',

    template: _.template('<a href="#"><%- name %></a>'),

    events: {
        'click': 'select',
    },

    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },

    select: function(e) {
        this.model.trigger('selected', this.model);
        e.preventDefault();
    }
});

SHOC.Scripts.MenuView = Backbone.View.extend({
    el: '#menu',

    events: {
        'click .manual a': 'selectManual'
    },

    initialize: function(options) {
        this.script = options.script;
        this.collection = new SHOC.Scripts.Collection();
        this.collection.on('add', this.add, this);
        this.collection.fetch();
        this.formview = new SHOC.Scripts.ItemForm();
    },

    add: function(script) {
        script.on('selected', this.selectScript, this);
        var view = new SHOC.Scripts.MenuItemView({model: script});
        this.$el.find('.script').last().after(view.render().el);

        // If this script is currently the active one, make sure to select it.
        if (script.get('name') === this.script) {
            this.selectScript(script);
        }
    },

    selectScript: function(script) {
        if (this.listview) this.listview.remove();
        var lines = script.get('lines');
        if (!lines.length) {
            $('.btn-start, .btn-repeat').addClass('disabled').parent().show();
        } else {
            $('.btn-start, .btn-repeat').removeClass('disabled').parent().show();
        }
        var collection = new SHOC.Scripts.ItemCollection(lines);
        this.listview = new SHOC.Scripts.ListView({
            collection: collection
        });
        this.formview.collection = collection;
        SHOC.camera.model.patch({script: script.get('name')}).done(function() {
            this.listview.render();
        }.bind(this));
        $('#scripts').show();
        $('#controls').hide();
        $('.script-status').show();

        $('#mode .dropdown-toggle strong').text(script.get('name'));
    },

    selectManual: function(e) {
        $('.btn-start').removeClass('disabled');
        $('.btn-repeat').addClass('disabled').parent().hide();
        $('#scripts').hide();
        $('#controls').show();
        $('.script-status').hide();

        $('#mode .dropdown-toggle strong').text('Manual');

        SHOC.camera.model.patch({script: null});

        e.preventDefault();
    }
});

jQuery(function($) {
    $('body').on('show.bs.modal', '#add-script', function(e) {
        new SHOC.Scripts.CreateForm({
            collection: SHOC.scripts.collection
        });
    });
});
