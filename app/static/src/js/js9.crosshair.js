/* Draws a crosshair on the JS9 canvas with x and y slice plots. */

(function() {
    "use strict";

    // Save a reference to the old displayMessage function.
    var displayMessage = JS9.Image.prototype.displayMessage;

    var imexam = require('./imexam');

    function init() {
        this.display.newShapeLayer('crosshair', {
            canvas: {
                zindex: JS9.SHAPEZINDEX + 4
            }
        });

        this.canvas = document.getElementById('JS9-crosshair-shapeLayer');
        this.context = this.canvas.getContext('2d');

        // We want to keep track of when a user has clicked somewhere on the
        // canvas to lock the crosshairs in position.
        this.locked = false;

        // Override the built-in displayMessage to provide our own
        // functionality -- we don't want to upate the info message when
        // moving the cursor over the canvas while the crosshairs are locked.
        var that = this;
        JS9.Image.prototype.displayMessage = function(type, message) {
            if (!that.locked) {
                displayMessage.apply(this, arguments);
            }
        };
    }

    function draw(crosshair) {
        this.context.lineWidth = 1;

        // Remove the previous crosshairs by clearing the canvas.
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

        // Make it obvious to the user when the crosshairs have been locked.
        this.context.strokeStyle = (this.locked ? '#00ff00' : '#ff0000');

        // Draw X line.
        this.context.beginPath();
        this.context.moveTo(0, crosshair.y);
        this.context.lineTo(crosshair.width, crosshair.y);
        this.context.stroke();
        this.context.closePath();

        // Draw Y line.
        this.context.beginPath();
        this.context.moveTo(crosshair.x, 0);
        this.context.lineTo(crosshair.x, crosshair.height);
        this.context.stroke();
        this.context.closePath();
    }

    // Updates slices according to the current locked position.
    function plot(image) {
        var image2d = imexam.ndops.ndarray(
            image.raw.data, [image.raw.height, image.raw.width]);

        // Get X slice.
        var xSlice = [],
            y = Math.floor(this.position.y);
        for (var x = image.rgb.sect.x0, i = 1; x < image.rgb.sect.x1; x++, i++) {
            xSlice.push([i, image2d.get(y, x)]);
        }
        this.xPlot.getAxes().xaxis.options.max = xSlice.length;
        this.xPlot.setData([{
            data: xSlice,
            color: '#428bca'
        }]);

        // Get Y slice.
        var ySlice = [],
            x = Math.floor(this.position.x);
        for (var y = image.rgb.sect.y0, i = 1; y < image.rgb.sect.y1; y++, i++) {
            ySlice.push([image2d.get(y, x), i]);
        }
        this.yPlot.getAxes().yaxis.options.max = ySlice.length;
        this.yPlot.setData([{
            data: ySlice,
            color: '#428bca'
        }]);

        // Append min and max values to the info message.
        var xaxis = this.xPlot.getAxes().yaxis;
        var yaxis = this.yPlot.getAxes().xaxis;

        var message = JS9.GetValPos(this.position);
        if (message) {
            var message = message.vstr
            message += sprintf(' min(%d, %d)', xaxis.min, yaxis.min);
            message += sprintf(' max(%d, %d)', xaxis.max, yaxis.max);
            displayMessage.call(this, 'info', message);
        }

        var that = this;
        requestAnimationFrame(function() {
            that.xPlot.setupGrid();
            that.xPlot.draw();

            that.yPlot.setupGrid();
            that.yPlot.draw();
        });
    }

    function onMouseMove(image, position, e) {
        if (this.locked) return;

        draw.call(this, {
            width: image.display.width,
            height: image.display.height,
            x: e.layerX,
            y: e.layerY
        });
    }

    // Hides the crosshairs if they're not locked.
    function onMouseOut(image, position, e) {
        if (this.locked) return;

        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    // Records the position where the mouse was clicked.
    function onMouseDown(image, position, e) {
        this.dragstart = position;
    }

    function onMouseUp(image, position, e) {
        // Between mousedown and mouseup events, the user may have intended to
        // adjust the contrast by dragging the mouse so we don't do anything.
        if (this.dragstart.x != position.x && this.dragstart.y != position.y) return;

        // We need to be inside the bounds of the current image to lock the crosshairs.
        if (!this.locked) {
            if(!((position.x > 0) && (position.y > 0) &&
                    (position.x < image.rgb.sect.x1) &&
                    (position.y < image.rgb.sect.y1))) {
                return;
            }
        }

        this.locked = !this.locked;

        if (this.locked) {
            // Save the image position so we can convert it back to the display
            // position when panning or zooming.
            this.position = position;

            // Update slices according to current locked position.
            plot.call(this, image);
        }

        // Jump to where the mouse was clicked.
        draw.call(this, {
            width: image.display.width,
            height: image.display.height,
            x: e.layerX,
            y: e.layerY
        });
    }

    // Initialises the plots with no data when a new image is loaded.
    function onImageLoad(image) {
        this.xPlot = $.plot('#x-slice', [], {
            grid: {
                margin: {
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0
                },
                borderWidth: 1,
                minBorderMargin: 0
            },
            xaxis: {
                show: false,
                min: 0,
                max: image.raw.width,
                tickLength: 0
            },
            yaxis: {
                show: false
            }
        });

        this.yPlot = $.plot('#y-slice', [], {
            grid: {
                margin: {
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0
                },
                borderWidth: 1,
                minBorderMargin: 0
            },
            xaxis: {
                show: false
            },
            yaxis: {
                show: false,
                min: 0,
                max: image.raw.height,
                tickLength: 0,
                position: 'right'
            }
        });
    }

    // Updates crosshairs and plots when panning, zooming or refreshing an image.
    function onImageDisplay(image) {
        // We don't need to do anything if the crosshair isn't locked.
        if (!this.locked) {
            if (image.valpos) {
                // Calculate an updated info message and display it.
                var x = image.valpos.ix,
                    y = image.valpos.iy;

                // We need to clear the current message otherwise JS9 will just
                // keep displaying the same values until the mouse is moved.
                image.valpos = null;
                image.valpos = image.updateValpos({ x: x, y: y});
            }

            return;
        }

        plot.call(this, image);

        // In the case where the crosshairs are locked in position, we need to
        // force JS9 to display the values at that position regardless of the
        // cursor's current position.
        image.valpos = null;
        image.updateValpos({
            x: this.position.x,
            y: this.position.y
        });

        // Update crosshair position on zoom/pan events.
        var position = image.imageToDisplayPos(this.position);
        draw.call(this, {
            width: image.display.width,
            height: image.display.height,
            x: position.x,
            y: position.y
        });
    }

    JS9.RegisterPlugin('SHOC', 'crosshair', init, {
        onmousemove: onMouseMove,
        onmouseout: onMouseOut,
        onmousedown: onMouseDown,
        onmouseup: onMouseUp,
        onimageload: onImageLoad,
        onimagedisplay: onImageDisplay,
        winDims: [0, 0]
    });

})();
