import uuid
import copy
import pickle

from http.client import NOT_FOUND, BAD_REQUEST, CREATED
from flask import Blueprint, request
from app import shoc, jsonify, login_required
from . import model
from .models import SCRIPTS


api = Blueprint('camera_scripts_api', __name__)


def translate(line):
    """Translate a script line with indices to values."""

    # XXX: Workaround for when no filter wheel devices are attached. When there
    # are no filter wheels attached, the controller sets shoc.filterwheel to an
    # empty dict which doesn't have a "get_state" attribute.
    try:
        wheels = shoc.filterwheel.get_state()
    except AttributeError:
        wheels = {}

    def get_filter(wheel_name, filter_id):
        # Filter ID is set to None for scripts that were created when only one
        # filter wheel was installed so we need to handle that as a special case.
        if filter_id is not None:
            return wheels[wheel_name].filters[int(filter_id)-1][0]
    line = copy.deepcopy(line)
    line.update({
        'horizontal_shift_speed': model.settings['horizontal_shift_speed'][int(line['horizontal_shift_speed'])],
        'preamp_gain': model.settings['preamp_gain'][int(line['preamp_gain'])],
        'binning': model.settings['binning'][int(line['binning'])],
        'sub_image': model.settings['sub_image'][int(line['sub_image'])],
    })
    for wheel in wheels:
        line['filters'][wheel] = get_filter(wheel, line['filters'][wheel])
    return line


@api.route('/', methods=['GET'])
@api.route('/<name>', methods=['GET'])
@login_required
def get_scripts(name=None):
    scripts = {}
    for script_name, lines in SCRIPTS.items():
        scripts[script_name] = {
            'name': script_name,
            'lines': [translate(line) for line in lines]
        }

        if name == script_name:
            return jsonify(scripts[name])

    if name is None:
        return jsonify(list(scripts.values()))

    return jsonify(), NOT_FOUND


@api.route('/<name>', methods=['PUT'])
@login_required
def save_script(name):
    SCRIPTS[name] = []
    return jsonify(), CREATED


@api.route('/<name>/lines', methods=['PUT', 'POST'])
@login_required
def save_script_line(name):
    if not request.json:
        return jsonify(), BAD_REQUEST

    line = request.json
    line['id'] = uuid.uuid4().hex

    try:
        script = SCRIPTS[name]
    except IndexError:
        return jsonify(), NOT_FOUND
    else:
        script.append(line)

    with open('scripts.pickle', 'wb') as f:
        pickle.dump(SCRIPTS, f)

    # Return a translated representation of the received data. This will make
    # the Backbone model use the translated values instead so that users see
    # "Ultraviolet" instead of a filter index for example.
    return jsonify(translate(line)), CREATED


@api.route('/<name>/lines/<uuid>', methods=['DELETE'])
@login_required
def delete_script_line(name, uuid):
    if name not in SCRIPTS:
        return jsonify(), NOT_FOUND

    for index, line in enumerate(SCRIPTS[name]):
        if line['id'] == uuid:
            del SCRIPTS[name][index]
            break
    else:
        return jsonify(), NOT_FOUND

    with open('scripts.pickle', 'wb') as f:
        pickle.dump(SCRIPTS, f)

    return jsonify()
