from flask_wtf import Form
from wtforms import (SelectField, IntegerField, FloatField, StringField,
    BooleanField, RadioField, FormField)
from wtforms.validators import InputRequired


TRIGGER_MODE_EXTERNAL = 1
TRIGGER_MODE_CHOICES = [
    (0, 'Internal'),
    (1, 'External'),
    (6, 'External Start'),
]


SUB_IMAGE_FULL = 0
SUB_IMAGE_CHOICES = [
    (0, '1024x1024 (Full)'),
    (1, '512x512'),
    (2, '256x256'),
    (3, '128x128'),
    (4, '64x64'),
    (5, 'Custom'),
]


BINNING_NONE = 0
BINNING_CHOICES = [
    (0, '1x1'),
    (1, '2x2'),
    (2, '4x4'),
    (3, '8x8'),
    (4, '16x16'),
    (5, 'Custom'),
]


OUTPUT_AMPLIFIER_EM = 0
OUTPUT_AMPLIFIER_CHOICES = [
    (0, 'EM'),
    (1, 'Conventional'),
]


OBSERVATION_TYPES = [
    ('', ''),
    ('bias', 'Bias'),
    ('flat', 'Flat'),
    ('dark', 'Dark'),
    ('object', 'Object'),
]


class ObservationForm(Form):
    observer = StringField('Observer')
    obstype = SelectField('Observation Type',
        choices=OBSERVATION_TYPES, default='')
    object = StringField('Object')
    objra = StringField('Right Ascension')
    objdec = StringField('Declination')
    objepoch = StringField('Epoch')
    objequin = StringField('Equinox')


class CameraImageForm(Form):
    sub_image = SelectField('Sub Image', validators=[InputRequired()],
        choices=SUB_IMAGE_CHOICES,
        default=SUB_IMAGE_FULL, coerce=int)
    sub_image_left = IntegerField('Left', default=1)
    sub_image_bottom = IntegerField('Bottom', default=1)
    sub_image_right = IntegerField('Right', default=1024)
    sub_image_top = IntegerField('Top', default=1024)

    binning = SelectField('Binning', validators=[InputRequired()],
        choices=BINNING_CHOICES,
        default=BINNING_NONE, coerce=int)
    binning_vertical = IntegerField('Vertical', default=1)
    binning_horizontal = IntegerField('Horizontal', default=1)


class CameraForm(Form):
    trigger_mode = SelectField('Triggering', validators=[InputRequired()],
        choices=TRIGGER_MODE_CHOICES, default=TRIGGER_MODE_EXTERNAL,
        coerce=int)
    exposure_time = FloatField('Exposure Time', validators=[InputRequired()],
        default=0.1, description='Seconds')
    target_temperature = IntegerField('Temperature',
        validators=[InputRequired()],
        default=0, description='Degrees Celsius')

    kinetic_series_length = IntegerField('Kinetic Series Length', default=1)
    kinetic_cycle_time = FloatField('Kinetic Cycle Time', default=0)

    image = FormField(CameraImageForm)

    start_index = StringField('Start Index', default='1')

    horizontal_shift_speed = SelectField('Readout Rate')
    preamp_gain = SelectField('Pre-Amplifier Gain')
    em_gain = IntegerField('Electron Multiplier Gain', default=0)
    output_amplifier = RadioField('Output Amplifier',
        choices=OUTPUT_AMPLIFIER_CHOICES,
        default=OUTPUT_AMPLIFIER_EM, coerce=int)

    flip_x = RadioField('Flip X Axis',
        choices=[(0, 'OFF'), (1, 'ON')], coerce=int)
    flip_y = RadioField('Flip Y Axis',
        choices=[(0, 'OFF'), (1, 'ON')], coerce=int)

    observation = FormField(ObservationForm)

    script_iterations = IntegerField('Script Iterations', default=1)
