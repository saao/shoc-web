from io import StringIO
from io import BytesIO

from geventwebsocket import WebSocketError

from http.client import NOT_FOUND, BAD_REQUEST
from flask import Blueprint, Response, render_template, request, jsonify
from app import sockets, shoc, login_required
from .models import Camera
from .forms import CameraForm


views = Blueprint('camera', __name__)
model = Camera()


@views.route('/', methods=['GET'])
@login_required
def index():
    # if not model.initialized:
    #     return render_template('camera/initialize.html')

    form = CameraForm(**model.state)

    form.horizontal_shift_speed.choices = []
    for k, v in enumerate(model.horizontal_shift_speeds):
       form.horizontal_shift_speed.choices.append((k, v))

    form.preamp_gain.choices = []
    for k, v in enumerate(model.preamp_gains):
        form.preamp_gain.choices.append((k, v))

    # XXX: Workaround for when no filter wheel devices are attached. When there
    # are no filter wheels attached, the controller sets shoc.filterwheel to an
    # empty dict which doesn't have a "get_state" attribute.
    try:
        wheels = shoc.filterwheel.get_state()
    except AttributeError:
        wheels = {}

    return render_template('camera/index.html', form=form, model=model,
        wheels=[w.__dict__ for w in wheels.values()],
        gains=model.settings['preamp_gain'],
        rates=model.settings['horizontal_shift_speed'])


api = Blueprint('camera_api', __name__)


@api.route('', methods=['GET'])
@login_required
def get():
    return jsonify(model.state)


@api.route('', methods=['PATCH'])
@login_required
def patch():
    print("Sending the patch")
    if not request.json:
        return jsonify(), BAD_REQUEST

    for key, value in request.json.items():
        print(f"Key = {key}  Val = {value}") 
        setattr(model, key, value)

    return jsonify(model.state)


@sockets.route('/data')
def data(ws):
    while True:
        message = ws.receive()

        # Fetch an image from the camera.
        image = shoc.camera.get_image()

        if image:
            # Send the image over the websocket.
            #f = StringIO()
            f = BytesIO()
            image.writeto(f)

            f.seek(0)

            try:
                ws.send(f.getvalue(), binary=True)
            except WebSocketError as e:
                ws.logger.error(e)
                ws.close()
                break
        else:
            # If the camera doesn't return an image, send an empty response to
            # the browser.
            try:
                ws.send('')
            except WebSocketError as e:
                ws.logger.error(e)
                ws.close()
                break
