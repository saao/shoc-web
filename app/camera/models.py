import gevent

from gevent.event import Event
from flask import current_app, g
from app import shoc, SCRIPTS, ValidationError


SUB_IMAGE_OPTIONS = {
    (1, 1, 1024, 1024): 0,
    (1, 1, 512, 512): 1,
    (1, 1, 256, 256): 2,
    (1, 1, 128, 128): 3,
    (1, 1, 64, 64): 4,
}
BINNING_OPTIONS = {
    (1, 1): 0,
    (2, 2): 1,
    (4, 4): 2,
    (8, 8): 3,
    (16, 16): 4,
}


KEYWORD_COMMENTS = {
    'observer': 'Observer who acquired the data',
    'obstype': 'Observation type',
    'object': 'IAU name of observed object',
    'objra': 'Right ascension of object',
    'objdec': 'Declination of object',
    'objepoch': 'Object coordinate epoch',
    'objequin': 'Object coordinate equinox',
}


BINNING = [
    {'binning_horizontal': 1, 'binning_vertical': 1},
    {'binning_horizontal': 2, 'binning_vertical': 2},
    {'binning_horizontal': 3, 'binning_vertical': 3},
    {'binning_horizontal': 4, 'binning_vertical': 4},
    {'binning_horizontal': 5, 'binning_vertical': 5},
    {'binning_horizontal': 6, 'binning_vertical': 6},
    {'binning_horizontal': 7, 'binning_vertical': 7},
    {'binning_horizontal': 8, 'binning_vertical': 8},
    {'binning_horizontal': 9, 'binning_vertical': 9},
    {'binning_horizontal': 10, 'binning_vertical': 10},
    {'binning_horizontal': 16, 'binning_vertical': 16},
]
IMAGE = [
    {'sub_image_right': 1024, 'sub_image_top': 1024},
    {'sub_image_right': 512, 'sub_image_top': 512},
    {'sub_image_right': 256, 'sub_image_top': 256},
    {'sub_image_right': 128, 'sub_image_top': 128},
    {'sub_image_right': 64, 'sub_image_top': 64},
]


# XXX: These values are for conventional mode only.
CAMERA = {
    6003: {
        'horizontal_shift_speed': [
            '10.0MHz at 14-bit',
            '5.0MHz at 14-bit',
            '3.0MHz at 14-bit',
            '1.0MHz at 16-bit',
        ],
        'preamp_gain': ['1.0', '2.2', '4.6'],
        'binning': [
            '1x1',
            '2x2',
            '3x3',
            '4x4',
            '5x5',
            '6x6',
            '7x7',
            '8x8',
            '9x9',
            '10x10',
            '16x16'
        ],
        'sub_image': ['1024x1024', '512x512', '256x256', '128x128', '64x64'],
    },
    5982: {
        'horizontal_shift_speed': [
            '3MHz (14bit)',
            '1MHz (16bit)',
        ],
        'preamp_gain': ['1.0', '2.4', '4.9'],
        'binning': [
            '1x1',
            '2x2',
            '3x3',
            '4x4',
            '5x5',
            '6x6',
            '7x7',
            '8x8',
            '9x9',
            '10x10',
            '16x16'
        ],
        'sub_image': ['1024x1024', '512x512', '256x256', '128x128', '64x64'],
    },
    12811: {
        'horizontal_shift_speed': [
            '1MHz (16bit)',
            '0.1MHz (16bit)',
        ],
        'preamp_gain': ['1.0', '2.0'],
        'binning': [
            '1x1',
            '2x2',
            '3x3',
            '4x4',
            '5x5',
            '6x6',
            '7x7',
            '8x8',
            '9x9',
            '10x10',
            '16x16'
        ],
        'sub_image': ['1024x1024', '512x512', '256x256', '128x128', '64x64'],
    },
    6448: {
        'horizontal_shift_speed': [
            '3MHz (14bit)',
            '1MHz (16bit)',
        ],
        'preamp_gain': ['1.0', '2.5', '5.2'],
        'binning': [
            '1x1',
            '2x2',
            '3x3',
            '4x4',
            '5x5',
            '6x6',
            '7x7',
            '8x8',
            '9x9',
            '10x10',
            '16x16'
        ],
        'sub_image': ['1024x1024', '512x512', '256x256', '128x128', '64x64'],
    },
}


def execute_script(camera):
    script = SCRIPTS.get(camera._script, [])
    for i in range(1, camera.script_iterations+1):
        for index, line in enumerate(script, start=1):
            if camera.aborting.is_set():
                break
            camera._script_progress = (index, i)

            # XXX: Workaround for when no filter wheel devices are attached.
            # When there are no filter wheels attached, the controller sets
            # shoc.filterwheel to an empty dict which doesn't have a
            # "get_state" attribute.
            try:
                wheels = shoc.filterwheel.wheels.items()
            except AttributeError:
                wheels = {}

            for name, wheel in wheels:
                try:
                    position = line['filters'][name]
                except IndexError:
                    continue # No filter configured for this wheel.

                # Initialize the wheel.
                if not wheel.initialized:
                    shoc.filterwheel.initialize(name)

                    # XXX: Give the wheel a second to start moving. Without
                    # this delay we skip the check to see if the wheel is still
                    # moving and the FITS headers are populated with the
                    # previous values.
                    gevent.sleep(1)

                    # Wait for the wheel to stop moving before we try to move it to
                    # the required position.
                    while shoc.filterwheel.wheels[name].moving:
                        gevent.sleep(1)

                if wheel.position != position:
                    shoc.filterwheel.move(name, position)

                    # XXX: Give the wheel a second to start moving. Without
                    # this delay we skip the check to see if the wheel is still
                    # moving and the FITS headers are populated with the
                    # previous values.
                    gevent.sleep(1)

                    # Wait for the wheel to stop moving.
                    while shoc.filterwheel.wheels[name].moving:
                        gevent.sleep(1)

            camera.trigger_mode = 0  # XXX: We're always running in internal.
            camera.exposure_time = line['exposure_time']
            camera.kinetic_series_length = line['kinetic_series_length']
            image = {
                'sub_image_left': 1,
                'sub_image_bottom': 1,
            }
            image.update(BINNING[line['binning']])
            image.update(IMAGE[line['sub_image']])
            camera.image = image
            camera.horizontal_shift_speed = line['horizontal_shift_speed']
            camera.preamp_gain = line['preamp_gain']

            shoc.start_exposure({
                k.upper(): {v: KEYWORD_COMMENTS[k]} for k, v in camera._observation.items()})
            camera._script_running = True

            # Wait until we're done acquiring data.
            while camera.acquiring:
                gevent.sleep(1)

            # Give the UI some time to realise that we're done with this line.
            gevent.sleep(1)

    camera._script_running = False


class Camera(object):
    def __init__(self):
        self._script = None
        self._script_progress = None
        self._observation = {
            'observer': '',
            'obstype': '',
            'object': '',
            'objra': '',
            'objdec': '',
            'objepoch': '',
            'objequin': '',
        }

        self._script_running = False
        self.script_iterations = 1

    @property
    def settings(self):
        return CAMERA[shoc.camera.serial]

    def set_script(self, name):
        self._script = name
    script = property(None, set_script)

    @property
    def initialized(self):
        return shoc.camera.is_initialized()

    @initialized.setter
    def initialized(self, value):
        if value:
            shoc.camera.initialize()
        else:
            shoc.camera.switch_off()

    @property
    def cooler(self):
        print("Returning cooler")
        return shoc.camera.state['cooler_on']

    @cooler.setter
    def cooler(self, cooler):
        print(f"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Setting cooler with {cooler}")
        if cooler:
            shoc.camera.set_cooler_on()
        else:
            shoc.camera.set_cooler_off()

    @property
    def acquiring(self):
        return shoc.camera.state['acquiring']

    @acquiring.setter
    def acquiring(self, acquiring):
        self.aborting = Event()
        if acquiring:
            if self._script:
                g = gevent.spawn(execute_script, self)
                try:
                    g.get()
                except Exception as e:
                    self._script_running = False
                    self.aborting.set()
                    shoc.camera.abort()
                    raise
            else:
                shoc.start_exposure({
                    k.upper(): {v: KEYWORD_COMMENTS[k]} for k, v in self._observation.items()})
        else:
            self._script_running = False
            self.aborting.set()
            shoc.camera.abort()

    @property
    def state(self):
        state = getattr(g, 'shoc_camera_state', None)
        if state is not None:
            return state

        current_state = shoc.camera.state

        # The camera may have turned off by now, so none of the rest of the
        # state is available right now.
        if not current_state['is_initialized']:
            return {'initialized': False}

        # These are given 'None' as the default in the controller which ends up
        # messing with the defaults that we need on the form.
        trigger_mode = current_state.get('trigger_mode', 1)
        if trigger_mode is None:
            trigger_mode = 1

        output_amplifier = current_state.get('output_amplifier')
        if output_amplifier is None:
            output_amplifier = 0

        left = current_state['subframe'].get('hstart', 1)
        bottom = current_state['subframe'].get('vstart', 1)
        right = current_state['subframe'].get('hend', 1024)
        top = current_state['subframe'].get('vend', 1024)
        sub_image = SUB_IMAGE_OPTIONS.get((left, bottom, right, top), 5)

        vertical = current_state['binning'].get('vbin', 1)
        horizontal = current_state['binning'].get('hbin', 1)
        binning = BINNING_OPTIONS.get((vertical, horizontal), 5)

        # Figure out the selected horizontal shift speed option based on the
        # channel and horizontal shift speed.
        horizontal_shift_speed = 1
        channel = current_state['ad_channel']
        hspeed = current_state['horizontal_shift_speed_index']
        for index, speeds in enumerate(self._horizontal_shift_speeds()):
            if (channel, hspeed) == speeds[0]:
                horizontal_shift_speed = index
                break

        print(f"Current_state:")
        print(current_state)
        
        cooler_on = current_state['cooler_on']
            
        # When in external mode, display the minimum kinetic cycle time
        # instead.
        kinetic_cycle_time = current_state['kinetic_cycle_time']
        if trigger_mode == 1:
            kinetic_cycle_time = current_state['minimum_kinetic_cycle_time']

        acquiring = self.acquiring or self._script_running

        # We're basically creating our own state here based on some of the
        # things that are set in the controller. The reason for doing this is
        # that there is no clean mapping between the state in the controller
        # and the data required by the user interface. Ideally we shouldn't
        # have to do this.
        state = {
            'initialized': self.initialized,
            'acquiring': acquiring,
            'acquisition_mode': current_state['acquisition_mode'],
            'trigger_mode': trigger_mode,
            'exposure_time': round(current_state['exposure_time'], 5),
            'temperature': current_state['temperature'],
            'temperature_state': current_state['temperature_state'],
            'target_temperature': int(current_state['target_temperature']),
            'kinetic_series_length': current_state['kinetic_series_length'],
            'kinetic_cycle_time': round(kinetic_cycle_time, 3),

            'cooler_on': int(cooler_on),

            'image': {
                'sub_image': sub_image,
                'sub_image_left': left,
                'sub_image_bottom': bottom,
                'sub_image_right': right,
                'sub_image_top': top,

                'binning': binning,
                'binning_vertical': vertical,
                'binning_horizontal': horizontal,
            },

            'start_index': self.start_index,
            'save_name': current_state['save_name'],

            'horizontal_shift_speed': horizontal_shift_speed,
            'horizontal_shift_speeds': self.horizontal_shift_speeds,
            'preamp_gain': current_state.get('preamp_gain', 0),
            'preamp_gains': self.preamp_gains,
            'em_gain': current_state.get('emccdgain', 0),
            'output_amplifier': output_amplifier,
            'images_acquired': self.images_acquired,
            'observation': self._observation,
            'script_progress': self._script_progress,
            'script_iterations': self.script_iterations,
        }

        flip = shoc.camera.get_image_flip()
        state.update({
            'flip_x': flip.x,
            'flip_y': flip.y,
        })

        # Cache state within the request context for repeated lookups in the
        # same request.
        g.shoc_camera_state = state

        return state

    def _horizontal_shift_speeds(self):
        hspeeds = shoc.camera.get_horizontal_shift_speeds()
        for channel, speeds in hspeeds.items():
            bit_depth = shoc.camera.get_bit_depth(channel)
            for speed_index, speed in speeds.items():
                yield (channel, speed_index), speed, bit_depth

    @property
    def horizontal_shift_speeds(self):
        options = []
        for lookup, speed, bit_depth in self._horizontal_shift_speeds():
            options.append('{}MHz at {}-bit'.format(speed, bit_depth))
        return options

    @property
    def horizontal_shift_speed(self):
        return shoc.camera.state.get('horizontal_shift_speed_index')

    @horizontal_shift_speed.setter
    def horizontal_shift_speed(self, speed):
        for index, speeds in enumerate(self._horizontal_shift_speeds()):
            if index == int(speed):
                channel, index = speeds[0]
                shoc.camera.set_horizontal_shift_speed(channel, index)
                break

        # Reset the preamp gain.
        self.preamp_gain = 0

    @property
    def preamp_gains(self):
        return shoc.camera.get_available_preamp_gains()

    def set_temperature(self, temperature):
        shoc.camera.set_temperature(int(temperature))
    temperature = property(None, set_temperature)

    def set_preview(self, value):
        if value:
            shoc.camera.start_continuous()
        else:
            shoc.camera.abort()
    preview = property(None, set_preview)

    @property
    def preamp_gain(self):
        return self.state['preamp_gain']

    @preamp_gain.setter
    def preamp_gain(self, gain):
        shoc.camera.set_preamp_gain(int(gain))

    @property
    def em_gain(self):
        return self.state['em_gain']

    @em_gain.setter
    def em_gain(self, gain):
        gain = int(gain)
        gain_range = shoc.camera.get_em_gain_range()
        low, high = gain_range.first, gain_range.second
        if gain < low or gain > high:
            errors = {'em_gain': 'Please select a value between {} and {}.'.format(low, high)}
            raise ValidationError('Validation error.', errors)
        shoc.camera.set_emccd_gain(gain)

    @property
    def available_output_amplifiers(self):
        return shoc.camera.get_num_available_output_amplifiers()

    @property
    def output_amplifier(self):
        return self.state['output_amplifier']

    @output_amplifier.setter
    def output_amplifier(self, amplifier):
        shoc.camera.set_output_amplifier(int(amplifier))

        # Reset the horizontal shift speed.
        self.horizontal_shift_speed = 0

    @property
    def trigger_mode(self):
        return self.state['trigger_mode']

    @trigger_mode.setter
    def trigger_mode(self, mode):
        shoc.camera.set_trigger_mode(int(mode))

    @property
    def exposure_time(self):
        return self.state['exposure_time']

    @exposure_time.setter
    def exposure_time(self, exposure_time):
        shoc.camera.set_exposure_time(float(exposure_time))

    @property
    def kinetic_series_length(self):
        return self.state['kinetic_series_length']

    @kinetic_series_length.setter
    def kinetic_series_length(self, length):
        shoc.camera.set_kinetic_series_length(int(length))

    @property
    def kinetic_cycle_time(self):
        return self.state['kinetic_cycle_time']

    @kinetic_cycle_time.setter
    def kinetic_cycle_time(self, cycle_time):
        shoc.camera.set_kinetic_cycle_time(float(cycle_time))

    @property
    def kinetic_cycle_time_hertz(self):
        if self.kinetic_cycle_time > 0:
            return round(1 / self.kinetic_cycle_time, 4)
        return 0

    @property
    def image(self):
        return self.state['image']

    @image.setter
    def image(self, image):
        binning_horizontal = int(image.get('binning_horizontal'))
        binning_vertical = int(image.get('binning_vertical'))

        sub_image_left = int(image.get('sub_image_left'))
        sub_image_right = int(image.get('sub_image_right'))
        sub_image_bottom = int(image.get('sub_image_bottom'))
        sub_image_top = int(image.get('sub_image_top'))

        shoc.camera.set_image(binning_horizontal, binning_vertical,
            sub_image_left, sub_image_right,
            sub_image_bottom, sub_image_top)

    @property
    def start_index(self):
        return shoc.camera.get_start_index()

    @start_index.setter
    def start_index(self, start_index):
        shoc.camera.set_start_index(int(start_index))

    @property
    def images_acquired(self):
        return shoc.camera.get_total_number_images_acquired()

    @property
    def observation(self):
        return self._observation

    @observation.setter
    def observation(self, observation):
        for k, v in observation.items():
            self._observation[k] = v.strip()

    def set_flip(self, flip):
        shoc.camera.set_image_flip(flip['x'], flip['y'])
    flip = property(None, set_flip)
