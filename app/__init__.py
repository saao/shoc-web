import json
import sys
import traceback
import logging
import pickle

from functools import wraps
from socket import timeout
from http.client import NOT_FOUND, INTERNAL_SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED
from flask import Flask, Response, render_template, current_app, request
from flask_babel import Babel
from flask_sockets import Sockets
from shoc.sdk.controller import Controller


# Load the camera scripts from disk. This should eventually move somewhere
# else, and should preferably use some other means of storage.
try:
    with open('scripts.pickle', 'rb') as f:
        SCRIPTS = pickle.load(f)
except IOError:
    SCRIPTS = {}


def jsonify(obj={}):
    return Response(json.dumps(obj), mimetype='application/json')


def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not (
                auth.username == current_app.config['AUTH_USERNAME']
                    and auth.password == current_app.config['AUTH_PASSWORD']):
            return Response('Please log in', UNAUTHORIZED,
                    {'WWW-Authenticate': 'Basic realm="Login Required"'})
        return f(*args, **kwargs)
    return decorated


class ValidationError(Exception):
    pass


class CustomResponse(Response):
    @classmethod
    def force_type(cls, rv, environ=None):
        """Serializes dicts to JSON."""
        if isinstance(rv, dict):
            rv = jsonify(rv)
        return super(CustomResponse, cls).force_type(rv, environ)


class SHOC(object):
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self._shoc = Controller(remote_host=app.config['SHOC_HOST'],
            telescope=app.config['TELESCOPE'])

    def __getattr__(self, name):
        if name == 'filterwheel':
            return self._shoc.fwc
        elif name == 'gps':
            return self._shoc.gpsc
        elif name == 'camera':
            return self._shoc.camc
        else:
            return getattr(self._shoc, name)


babel = Babel()
shoc = SHOC()
sockets = Sockets()


def create_app():
    print("Creating flask app")
    app = Flask(__name__)
    print("Created Flask app ")
    app.response_class = CustomResponse
    app.config.from_pyfile('settings.cfg')

    print("initing apps")
    babel.init_app(app)
    sockets.init_app(app)
    shoc.init_app(app)

    print("Apps inited")
    @app.errorhandler(timeout)
    def timeout_error(error):

        # if request.is_xhr:
        #     if app.config.get('DEBUG', False):
        #         return jsonify({'debug': traceback.format_exc()}), INTERNAL_SERVER_ERROR
        #     else:
        #         raise Exception('Your request timed out. Please try again.')
        return render_template(
            '500.html', error='Your request timed out.',
            traceback=traceback.format_exc()), INTERNAL_SERVER_ERROR

    @app.errorhandler(ValidationError)
    def validation_error(error):
        from flask import jsonify
        # if request.is_xhr:
        #     message, errors = error.args
        #     return jsonify(errors=errors), BAD_REQUEST
        raise error

    @app.errorhandler(Exception)
    def error(error):
        """Returns the traceback for ajax requests when in debug mode."""
        # if request.is_xhr and app.config.get('DEBUG', False):
        #     return jsonify({'debug': traceback.format_exc()}), INTERNAL_SERVER_ERROR
        exc_info = sys.exc_info()
        #raise exc_info[1], None, exc_info[2]
        raise exc_info[0].with_traceback(exc_info[1], exc_info[2])
    @app.errorhandler(NOT_FOUND)
    def not_found(error):
        return render_template('404.html'), NOT_FOUND

    @app.errorhandler(INTERNAL_SERVER_ERROR)
    def internal_error(error):
        # if request.is_xhr:
        #     return jsonify({'error': error.message}), INTERNAL_SERVER_ERROR
        return render_template(
            '500.html', error=error,
            traceback=traceback.format_exc()), INTERNAL_SERVER_ERROR

    from . import filterwheel
    from .filterwheel import api as filterwheel_api
    app.register_blueprint(filterwheel.views)
    app.register_blueprint(filterwheel_api, url_prefix='/instruments/wheels')

    from .gps import gps
    from .gps import api as gps_api
    app.register_blueprint(gps, url_prefix='/gps')
    app.register_blueprint(gps_api, url_prefix='/instruments')

    from . import camera
    from .camera import api as camera_api
    app.register_blueprint(camera.views, url_prefix='/camera')
    app.register_blueprint(camera.api, url_prefix='/instruments/camera')

    from .camera.scripts import api as script_api
    app.register_blueprint(script_api, url_prefix='/instruments/camera/scripts')

    return app
