from flask.ext.script import Manager, Server, Shell
from app import create_app


manager = Manager(create_app)
manager.add_command('runserver', Server(host='0.0.0.0', port=5000))
manager.add_command('shell', Shell())

@manager.command
def test():
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)

if __name__ == '__main__':
    manager.run()
